---
layout: page
title: "Ting $25 Discount"
date: 2014-04-06 23:18
comments: false
sharing: false
footer: true
---

So part of being a Ting customer is you can give our referrals to your friends and
families.  Every time someone uses your referral code, you get $25 and they get $25.
It's a win-win.

Just click the link below for my referral (or wait 5 seconds), and thanks!

[Ting.com](https://zc7c9715lq2.ting.com)

<div><script type="text/javascript">
    function redirect() {
      window.location = "https://zc7c9715lq2.ting.com/";
    };
    setTimeout(function(){redirect()}, 5000);
</script></div>

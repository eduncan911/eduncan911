---
layout: post
title: "Block Popups/Ads with a single file copy!"
date: 2005-08-20 22:03:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff"]
alias: ["/blog/block-popups-ads-with-a-single-file-copy.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>I've seen this before but haven't really gone back out to find an updated file to use since I've formatted a few times over the years.&nbsp; </P>
<P>There is a file on your machine called a "hosts" file.&nbsp; We use this a lot for programming websites.&nbsp; Well, you can use this file to block the majority of Spyware and PopUps that come up on your browser.&nbsp; </P>
<P>I know i know, "I already have a popup blocker".&nbsp; Yep, but do you have a "Ads" blocker?&nbsp; Blocking those annoying Flash and/or animated ads from annoying the hell out of you?&nbsp; This is what this file does.</P>
<P>I went out and found a fellow by the name of Mike that has been keeping an up-to-date file that you can download.&nbsp; Mike explains it best on his site:</P>
<P><A href="http://everythingisnt.com/hosts.html">http://everythingisnt.com/hosts.html</A></P>
<P>You can also just download the file manually here:</P>
<P><A href="http://everythingisnt.com/hosts">http://everythingisnt.com/hosts</A>&nbsp;(best to right-click and save as).&nbsp; </P>
<P>If you don't know what to do with this file, resort to the first link above.</P>
<P>&nbsp;</P>

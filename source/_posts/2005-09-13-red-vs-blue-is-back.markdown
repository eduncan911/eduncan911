---
layout: post
title: "Red vs. Blue is back!"
date: 2005-09-13 19:15:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff"]
alias: ["/blog/red-vs-blue-is-back.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<p><img height="354" src="http://files.redvsblue.com/web/images/web_season5_image3.jpg" style="width: 520px; height: 354px" width="520" />New episodes:</p><p><a href="http://www.redvsblue.com">www.redvsblue.com</a></p><p>Don&#39;t know what Red vs. Blue is?&nbsp; It is some of the funniest video spoofs on the net.&nbsp; It&#39;s actually a large series of episodes spoofing the popular Xbox game Halo (the original and Halo2, and I&#39;m sure Halo3 next year).&nbsp; </p><p>If you have ever played (and enjoyed) FPS games on the computer or consoles, especially online, you&nbsp;got to&nbsp;see these.&nbsp; If you are just starting, it&#39;s huge.&nbsp; They actually have seasons just like TV.&nbsp; Hours, no days worth of videos.&nbsp; So pace yourself.</p><p>&quot;It&#39;s not Pink!&nbsp; It&#39;s lightish Red.&quot;<br />&quot;Ok, don&#39;t get your panties in a waud.&quot;</p><p>&nbsp;</p>

---
layout: post
title: "Show your Work"
date: 2006-06-12 16:30:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Automotive", "Videos"]
alias: ["/blog/Show-your-Work.aspx", "/blog/show-your-work.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<p><img height="97" src="http://sjl-static3.sjl.youtube.com/vi/MgUDgNx-sBg/2.jpg" width="130" /></p><p>A friend of mine is pulling the motor out of his 240z he&#39;s going to be road racing.</p><p>He sent over a link today, that had a video of him &quot;Prepping&quot; for the pull.&nbsp; It shows a time-elapse segment of an entire day - from his garage.</p><p><a href="http://sccaforums.com/blogs/christoc/archive/2006/06/10/Motor_Pull_Preperation_240Z.aspx" target="_blank">http://sccaforums.com/blogs/christoc/archive/2006/06/10/Motor_Pull_Preperation_240Z.aspx</a></p><p>Kudos to Chris.&nbsp; Nice music from Cake too.</p><p>What a great idea!&nbsp; A cheap webcam, even wireless, computer in the house - record it all and post it on the net (insert: Automatically).&nbsp; </p><p>Does everyone know about Microsoft&#39;s PowerToys?&nbsp; There&#39;s one specifically for this called &quot;Webshot Timer&quot; I think.&nbsp; You can configure it to take a snapshot from your Webcam every X number of minutes, and it will even upload it to an FTP account for you.&nbsp; Think of it as a &quot;Live Webcam, with X minute delay&quot; as most people use it to overwrite a single image constantly.</p><p>You could take all of these, write them to a directory automatically.&nbsp; And at the end of the day, compile them into a video.&nbsp; :)</p><p>Wow, the possibilities are endless.</p><p>&nbsp;</p>

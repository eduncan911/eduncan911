---
layout: post
title: "Why the Inspiron is a King"
date: 2007-12-11 06:13:00 -0500
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Games"]
alias: ["/blog/why-the-inspiron-is-a-king.aspx","/archive/2007/12/11/why-the-inspiron-is-a-king.aspx"]
---

{% include imported_disclaimer.html %}

I step away from the system, hands nearly numb, being reminded again since 4 years why PCs will always out perform the latest consoles such as the Xbox 360 and Playstation 3. As I finish a two hour session of Battlefield 2 with my old buddies from [TCU](http://www.tcunit.com/), I can't help but to be amazed at the new found graphics tonight I have not seen before because my previous systems could not handle HDTV and full graphics.

<!-- more -->

[{% img /blog/archives/images/WhytheInspironisaKing_13F13/BF22007120815524590.png 'Battlefield2, 1920x1200x32, Ultra-High Hack, ~89 FPS' 'Battlefield2, 1920x1200x32, Ultra-High Hack, ~89 FPS' %}](/blog/archives/images/WhytheInspironisaKing_13F13/BF22007120815524590.png)

Stepping back, a bit dumb-founded, I question my sanity in the PC I just built tonight. I justify it quickly when I consider the tiny investment it took to bring this desktop to be the game-crushing machine it is now.  Only this time it is not my 4.2 Ghz 1200mhz FSB desktop - it's my $900 Dell Inspiron E1705 work laptop that I spent $240 upgrading. 

I have been a long-time advocate for Dell machines back from my IT Administrator days in Atlanta at CASE Masters and iDealmusic. Dell laptops in particular have had an interesting reign. Back in 1998, [PC Magazine](http://www.pcmag.com) started to issue the "Drop Test" against the leading laptops. Being dropped from six feet, all laptops failed to boot up except one - the Dell Latitude series which was a business-class unit. It was my first laptop I bought, and still have it. I was using it all the way up to 2005, lastly for automotive tuning. 

<p align="center"><img style="border-width: 0px;" border="0" alt="Dell Inspiron E1705/9400 Exploded View " width="470" height="585" mce_src="/blog/archives/images/WhytheInspironisaKing_13F13/exploded_view.jpg" src="/blog/archives/images/WhytheInspironisaKing_13F13/exploded_view.jpg"> <br><b>Dell Inspiron E1705/9400 Exploded View</b> (source: support.dell.com) </p>

<p>This story starts with the Dell Inspiron E1705 or also known as the 9400.&nbsp; Dell has done something great here, a laptop that is user-serviceable and user-upgradable.&nbsp; Yes it is a discontinued laptop, but if I only have know when I had it back at Telligent there would have been some serious modding going on.&nbsp; Tonight, I ventured into wild with a few components and upgrades not normally available.&nbsp; Each one has its own story to tell. 
</p><p align="center"><img style="border-width: 0px;" border="0" alt="E1705, 4 GB SDRAM, nVidia FX2500 7900GTX 512MB, Arctic Silver 5 " width="644" height="431" mce_src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0004.jpg" src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0004.jpg"> <br><b>E1705, 4 GB SDRAM, nVidia FX2500 7900GTX 512MB, Arctic Silver 5</b> </p>
<h3>4 GB SDRAM PC2-5400 667mhz</h3>
<p>The first thing many will cry fowl on here is, "The Inspiron E1705/9400 does not support 4GB of ram."&nbsp; This is untrue.&nbsp; While yes the bios only sees 3.25 GB available, so does the M17xx XPS systems and many desktop motherboards.&nbsp; There are lots of theories to this, but the truth is that your peripherals reserve that memory space and Windows allows them to over 3 GB - even if they never will use it.&nbsp; Basically, they are powered but never used.&nbsp; Using PAE causes some mobo/bios combinations to free up that address block to 4 GB, allowing for up 4.5 GB to be used.&nbsp; But the sad fact is there isn't much you can do right now when it's restricted at the hardware level.&nbsp; </p>

<p>Me?&nbsp; I figured it was a good $61 investment for my next laptop to have 4 GB as well.&nbsp; :) 
</p><h3>nVidia quadro FX2500m 512MB, 7900GTX GPU, PCI-E CAD and Gaming Card</h3>
<p>Another thing many will deny is the ability to stick an XPS or M90 "high-end" design cad and/or gaming card into the 'low-end" Inspiron business-class line.&nbsp; As you can see below, this is not the case either and the installation is pretty straight forward.&nbsp; The M90 designer-cad graphics card is the nVidia FX2500m quadro, which is a modified version of the 7900 GTX 512 MB (from my research).&nbsp; 
</p><p>The forum gurus are spending $350 to $500 on this particular card.&nbsp; I snagged mine from eBay for $160.&nbsp; The theory here was if it truly performs as well as the desktop version, then I can finally, after 4 years of planning, downgrade my PC to our Vista MCE machine in the main room and move to the laptop full-time (mobile again!).&nbsp; That is precisely what the PC was built for, less the water-cooling rig which actually is built to handle the heat demands for the next 5 to 7 years of CPUs.&nbsp; So bring 'em on! 
</p><h3>Dell 150w PA15 Power Adapter</h3>
<p>Most Dells these days come with an 90w.&nbsp; While this one sufficed for the first few hours of gaming, I did notice the unit getting much hotter than normal.&nbsp; Others amongst the net have mentioned that a 130w or 150w is needed because the 90w will burn out with these upgrades.&nbsp; So I ordered an original Dell PA15 new in box from eBay for $20.&nbsp; I needed a 2nd adapter anyhow.&nbsp; But be warned - they don't call these things a "brick" for no reason.&nbsp; This thing is literally the size of a brick! 
</p><h3>Overclocking</h3>
<p>I do not think I will be overclocking, but yes it is true.&nbsp; People are dramatically <a target="_blank" mce_href="http://www.notebookforums.com/forum153.html" href="http://www.notebookforums.com/forum153.html">overclocking their Dell</a> laptops.&nbsp; At this time the E1705's CPU, bus, and memory can not be overclocked (except the memory timings).&nbsp; But the GPU (graphics processor unit) can be if it is the nVidia series, and quite extensively too! 
</p><h3>Arctic Silver 5 Thermal Paste</h3>
<p>I only mention this because the other gurus on the forums have been putting this into their systems for the CPU upgrades.&nbsp; I have used AS5 for many years now, and I swear by it.&nbsp; My 4.2 Ghz desktop machine idles @ 28C and peaks at 37C with both cores maxed.&nbsp; That is just amazing (plus the water cooling). 
</p><p>So in this venture I decided to break out the old tube from the freezer and slap a dab on the CPU.&nbsp; The result was more than I expected with only 15 minutes of work, and well worth it.&nbsp; So let us begin the photo slide show with commen[dy]tary. 
</p><p>I was browsing one day and wanted a temperature reader for my laptop as I was noticing the fans kicking on high and the keyboard running quite hot at times.&nbsp; I stumbled upon <a target="_blank" mce_href="http://www.notebookforums.com/" href="http://www.notebookforums.com/">Notebook Forums</a> and was amazed at the level of upgrades people have done to their laptops from all brands.&nbsp; It seems the manufacturers have finally gave up and allowed end-users the ability to upgrade their machines.&nbsp; You can read up on your brand there. 
</p><p><b><font size="4">Reference</font></b> 
</p><p>For the Dell E1705 upgrades, there's numerous threads:</p>

<ul>
<li><a target="_blank" mce_href="http://www.notebookforums.com/showthread.php?t=151471&amp;highlight=7900gtx" href="http://www.notebookforums.com/showthread.php?t=151471&amp;highlight=7900gtx">Initial story of upgrading to the 7900 GTX</a> 
</li><li><a target="_blank" mce_href="http://www.notebookforums.com/thread185501.html" href="http://www.notebookforums.com/thread185501.html">Overclocking the nVidia cards</a> 
</li><li><a target="_blank" mce_href="http://www.notebookforums.com/thread205763.html" href="http://www.notebookforums.com/thread205763.html">Their own mini FAQs page, such as cooling and fan control</a>&nbsp;</li>
</ul>

<p><b><font size="4">Preface</font></b></p>

<p>This post is not meant to be a complete guide to installing the hardware.&nbsp; Instead it is more of a visual verification cue of some modifications (read cutting) that you must do.&nbsp; Check out Dell's support page and view/download the <a target="_blank" mce_href="http://support.dell.com/support/topics/global.aspx/support/my_systems_info/en/manuals?c=us&amp;cs=19&amp;l=en&amp;s=dhs" href="http://support.dell.com/support/topics/global.aspx/support/my_systems_info/en/manuals?c=us&amp;cs=19&amp;l=en&amp;s=dhs">service manual</a> as it has very detailed, step-by-step instructions to opening and servicing your laptop. </p>

<p><b><font size="4">So now let us break this thing open...</font></b></p>

<p align="center"><img style="border-width: 0px;" border="0" alt="Hinge Cover Removed " width="644" height="431" mce_src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0005.jpg" src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0005.jpg"> <br><b>Hinge Cover Removed</b> </p>

<p>First thing the service manual says is to remove the hinge cover as shown above.&nbsp; It was a bit nerve racking to pry that hard on a piece of plastic, and having it pop - sounding like I was breaking each and every tab.&nbsp; Being the first step in the entire process, I did have a reservation.&nbsp; But pushed on, prying harder, knowing it would come off cause the manual said so.&nbsp; 
</p><p>This is the most difficulty step.&nbsp; Get past the cracking, creaking, and popping sounds and you are home free. 
</p><p>Tip: If you do not have a plastic scribe, you can use a small/tiny flat-head screwdriver but do not pry up as the manual says because this will scratch the white plastic.&nbsp; Instead, use a twisting motion to pop the panel up and loose at the location of the red arrow.&nbsp; Then slowly work your way around the corner, twisting and popping the rest loose.&nbsp; Once you can get your finger under it, pull up and don't be scared. 
</p><p align="center"><img style="border-width: 0px;" border="0" alt="Keyboard Removed, ATI visible " width="644" height="431" mce_src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0007.jpg" src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0007.jpg"> <br><b>Keyboard Removed, ATI visible</b> </p>

<p>Next comes the keyboard.&nbsp; Nothing special here.&nbsp; I just wanted to show you the first glimpse of the ATI X1400 video card to be removed. 
</p><p align="center"><img style="border-width: 0px;" border="0" alt="Dell 1500 802.11n WiFi card and Antenna Leads " width="644" height="431" mce_src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0008.jpg" src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0008.jpg"> <br><b>Dell 1500 802.11n WiFi card and Antenna Leads</b> </p>

<p>Skipping ahead, along the way of removing the LCD panel it says to disconnect the wireless antenna of your wireless card.&nbsp; Here you can see the antenna leads connected to the Dell 1500 802.11n card via the red arrows.&nbsp; There is some questions on the web to even having to do this step if you do not have a wifi card.&nbsp; The answer is yes, they are just connected to dummy terminals. 
</p><p>And yes, if you put things back together and have no signal on your wifi card - you forgot to connect these! 
</p><p align="center"><img style="border-width: 0px;" border="0" alt="LCD Panel Removed " width="644" height="431" mce_src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0009.jpg" src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0009.jpg"> <br><b>LCD Panel Removed</b> </p>

<p>With the LCD removed, you've passed the point of no return (or now you are fully committed rather).&nbsp; 
</p><p align="center"><img style="border-width: 0px;" border="0" alt="Magnetized Screwdriver Helps " width="644" height="430" mce_src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0013.jpg" src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0013.jpg"> <br><b>Magnetized Screwdriver Helps</b> </p>

<p>This shows a little trick I have been using since I was a younglin'.&nbsp; I have a special set of tools that are magnetized, allowing me to remove (as shown) and install screws in very difficult places.&nbsp; Tools like this screwdriver pulls the screw right out, not having to flip over the system and let them drop and bounce. 
</p><p>It is very easy to magnetize your existing screwdrivers.&nbsp; <a target="_blank" mce_href="http://en.wikipedia.org/wiki/Watch_Mr._Wizard" href="http://en.wikipedia.org/wiki/Watch_Mr._Wizard">Mr. Wizard</a> showed me how when I was just a kid, and I have been doing it every since.&nbsp; Simply take a strong magnet and place it on the tip.&nbsp; Now slide it down the shaft, away from the tip in only one direction - do not twist the magnet around the shaft.&nbsp; This polarizes the shaft with the electrons that were resting at one end now oppositely charged at the other end of the shaft (because you moved the magnet).&nbsp; You can also de-magnetize by reversing precisely what you just did (place the magnet at the far end, and slide it towards the tip).&nbsp; This does not always work because you need to be using the same pole (North or South) of the magnet that when you started.&nbsp; Using the opposite pole will not have much effect.&nbsp; 
</p><p>Do not worry.&nbsp; Just slide the magnet down the shaft and be done.&nbsp; And do not magnetize all of your tools.&nbsp; Just two or three select screwdrivers, and keep them away from your other tools so they do not magnetize others.&nbsp; Note that I highly recommend the use of <a target="_blank" mce_href="http://www.thinkgeek.com/geektoys/science/770f/" href="http://www.thinkgeek.com/geektoys/science/770f/">rare-Earth magnets</a> as they are extremely strong. 
</p><p>And before you ask, yes I removed the HDD before using tools that were magnetized. 
</p><p align="center"><img style="border-width: 0px;" border="0" alt="Above: nVidia quadro FX2500m, Below: ATI X1400" width="644" height="431" mce_src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0015.jpg" src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0015.jpg"> <br><b>Above: nVidia quadro FX2500m, Below: ATI X1400</b></p>

<p align="left">Here is the view of the ATI X1400 still in the system and the new nVidia FX2500m for comparison.&nbsp; Notice there is no heat pipe on the left side of the ATI card, only the right side.&nbsp; </p>

<p align="center"><img style="border-width: 0px;" border="0" alt="Above: ATI X1400, Below: nVidia FX2500m " width="644" height="431" mce_src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0020.jpg" src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0020.jpg"> <br><b>Above: ATI X1400, Below: nVidia FX2500m</b> </p>

<p>Here you can see both cards and really see what others are calling "dual heat pipes" on the nVidia 7900 GTX (below). 
</p><p align="center"><img style="border-width: 0px;" border="0" alt="Modifying the case to fit the FX2500m" width="644" height="431" mce_src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0021.jpg" src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0021.jpg"> <br><b>Modifying the case to fit the FX2500m</b> </p>

<p>Now for the point of no return.&nbsp; To get the nVidia FX2500m to fit, you will need to modify the left-rear area of the laptop case behind the CPU's heatsink.&nbsp; This is because the left heatsink for the FX2500m is too wide to fit into this area.&nbsp; So you have to grind down or break off the two plastic sides/tabs.&nbsp; You can see in this photo above that I am grabbing one of the tabs to break off with a pair of needle-nose pliers. 
</p><p>Another option that would keep your full warranty would be to modify the FX2500m's heatsink by grinding down the two ends.&nbsp; I believe this would fit well with slight bending on the copper pipe to raise the heatsink about 1/8".&nbsp; This does not block air-flow for the cpu's heatsink, and still is vented to the rear outlet.&nbsp; Alas I do not have a grinder here, or I would have done it myself. 
</p><p>Do not worry about warranty here.&nbsp; If you are going this far, then you have the ability to service the laptop yourself if a component fails during your warranty period.&nbsp; Dell's "low cost" warranty is parts-only, and there ya go.&nbsp; Dell will send you the parts to replace, which you can service it yourself - under warranty.&nbsp; Just I would not take the chance of sending the entire laptop in for repair as they may catch the broken tabs. 
</p><p align="center"><img style="border-width: 0px;" border="0" alt="Right Tab Modified" width="644" height="431" mce_src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0024.jpg" src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0024.jpg"> <br><b>Right Tab Modified</b></p>

<p align="center"><img style="border-width: 0px;" border="0" alt="Left Tab Modified" width="644" height="431" mce_src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0023.jpg" src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0023.jpg"> <br><b>Left Tab Modified</b></p>

<p>You can see above it does not take very much.&nbsp; Test fit the FX2500m a few times though to make sure you removed enough. 
</p><p align="center"><img style="border-width: 0px;" border="0" alt="CPU &amp; Northbridge Heatpipe/Heatsink Removed " width="644" height="431" mce_src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0025.jpg" src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0025.jpg"> <br><b>CPU &amp; Northbridge Heatpipe/Heatsink Removed</b> </p>

While I had my system apart, I made plans to upgrade the CPU's cooling a little with a touch of Arctic Silver 5 as I noted above. You can see in this photo above the crap that Dell (and most PC makers) use on their CPUs. Mine had actually turned into a brittle paste. 

[{% img /blog/archives/images/WhytheInspironisaKing_13F13/IMG_0026.jpg 'Dell Inspiron E1705 CPU' %}](/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0026.jpg)

<p>Before you can apply AS5, you must thoroughly clean both surfaces (the CPU and heatsink).&nbsp; My best experiences has always been to use rubbing alcohol with some lint-free clothes or even kitchen paper towels.&nbsp; AS5 works smaller than you can see, filling the microscopic gaps and imperfections of the two pieces.&nbsp; Above you can see how the CPU's surface is now mirror-like, reflecting the white water-bottle cap.&nbsp; 
</p><p>The copper heatpipe was another story though.&nbsp; It was extremely rough and the paste was near-impossible to remove.&nbsp; Sorry I forgot to snap a photo of my work, because of the time involved I was just in a hurry to get it polished down.&nbsp; But I ended up using 400grit sandpaper, than 1000grit, and 2000grit to get a near-polished surface.&nbsp; You do not have to do any of this to your heatpipe though.&nbsp; Just get it cleaned off. 
</p><p>I went this route of applying AS5 and polishing my heatpipe because I did not like my CPU's temps under normal conditions just idling and/or working.&nbsp; I wanted to cool down the 62C idle of my CPU!&nbsp; I am proud to say that using AS5 did the trick.&nbsp; My idle temps are down to 37C with ambient about 74F.&nbsp; For comparison, my 4.2 Ghz desktop noted above idles at 28C but that's not fair given the custom cooling I did with it.&nbsp; But, it's close! 
</p><p align="center"><img style="border-width: 0px;" border="0" alt="CPU heatpipe and new (used) FX2500m all installed " width="644" height="431" mce_src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0028.jpg" src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0028.jpg"> <br><b>CPU heatpipe and new (used) FX2500m all installed</b> </p>

Here is a shot of everything re-installed and tightened down. Note in comparison to a photo above of the ATI X1400 in the system. 

[{% img /blog/archives/images/WhytheInspironisaKing_13F13/IMG_0030.jpg 'Dell Inspiron E1705 Fans' %}](/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0030.jpg)

Now this was something I was not happy about. There is a large gap from the left fan (fan 1) to the CPU's heatsink from before. Then there was now a gap between the CPU and GPU's heatsinks.You can see in the picture above the screwdriver that is clearly visible (click the image for the full-res version).

<p>In my history of custom cooling back in the 80s and 90s, there is one thing I learned that always remains constant: airflow is key.&nbsp; So my task would be to modify the airflow to force all air through the heatsinks by not allowing any to escape the airflow path through any gaps. 
</p><p align="center"><img style="border-width: 0px;" border="0" alt="Aluminum Foil Shrouds" width="644" height="431" mce_src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0031.jpg" src="/blog/archives/images/WhytheInspironisaKing_13F13/IMG_0031.jpg"> <br><b>Aluminum Foil Shrouds</b></p>

<p>I accomplished this by using a few pieces of kitchen aluminum foil, scotch-tape, and additional AS5.&nbsp; There are multiple reasons for this.&nbsp; First for safety I evaluated the entire area and found no electrical components or circuit boards anywhere near this area.&nbsp; You do not want a piece coming loose and short-circuiting things!&nbsp; </p>

Second is having aluminum foil allows the heat to transfer from one copper pipe to the other (just a tad though, too thin really to do a massive amount).That may not have been the best idea at first because CPUs usually run cooler than GPUs from my overclocking experience. But considering how freakin' high my CPU temps were before I even began, I could only see this as helping to offload some heat to the GPU's heatpipe - even if just a tad amount, anything had to help. 

I also closed down the air escaping on the left side of fan's outlet to the CPU's heatsink, as well as the gaps between the two heatsinks.&nbsp; Now all air is forced by the fan trough the CPU heatsink, and then through the GPU's heatsink where it than only has room to exit the back vent. 

[{% img /blog/archives/images/WhytheInspironisaKing_13F13/idle.jpg 'Dell Inspiron E1705 CPU Temps' 'Dell Inspiron E1705 CPU Temps' %}](/blog/archives/images/WhytheInspironisaKing_13F13/idle.jpg)
**CPU idle at 36C using Arctic Silver 5, down from 62C when OEM**

It is very well worth it, even if I did not upgrade the GPU, because now my CPU temps are a steady 37C with fans set to low. With no fans, it creeps up to 47C. Compare that to the 62C idle before with fans on high, and I think I came out ahead.

[{% img /blog/archives/images/WhytheInspironisaKing_13F13/Prime95.jpg 'Dell Inspiron E1705 CPU Temps' 'Dell Inspiron E1705 CPU Temps' %}](/blog/archives/images/WhytheInspironisaKing_13F13/Prime95.jpg)

**CPU cores pegged reaches 72C after 30 minutes of stress-testing, down from much much higher**

And for you guys into max CPU heat testing, using an instance of Prime95 on each core yields a max temp of 72C with 74F ambient after about 30 minutes. Much better than before! I will admit I did not pay a lot of attention to the max temp before the AS5 was applied. Let us just say I did not stress it for very long once it climbed to 85C.

While gaming the GPU hovered around 63 to 65C tonight after about two hours, and peaked at 70C just for an instant. I would have applied AS5 to the GPU's heatsink but I did not have a torx-bit small enough (it was missing from my collection) and I wanted to get my laptop running again after spending that time sanding down the CPU's heatpipe. 

In conclusion, I feel this was very much worth the effort. A few hours of research over a few days time, finding a great deal on the video card, and planning/preparation before hand, and it all took about 3 hours from start to end (including the sanding of the CPU's heatpipe). 

Not a bad system for under $1,200 with BF2 averaging in Fraps at 85 FPS and BF2142 around 78 FPS steady. Why someone would pay for an XPS system is beyond me. Just get an Inspiron for a good price, hunt around after a few months find a deal on the upgrade parts.

Now, if I can get my hands on one of those newer 1720/1730s with dual HDD bays... 

---
layout: post
title: "Bitcoin's IRS-blocking 'Dark Wallet' Launched Today"
date: 2014-05-01 14:38:13 -0400
comments: true
categories: technology
tags: ["Bitcoin", "Cryptography"]
---
[Hyper-Anonymising Bitcoin Service ‘Dark Wallet’ Launches Today](http://www.coindesk.com/hyper-anonymising-bitcoin-service-dark-wallet-launches-today/)

{% youtube Ouo7Q6Cf_yc %}

I first heard about this Dark Wallet protocol a few months ago when [New York state](http://pando.com/2014/03/12/moving-on-up-bitcoin-gets-a-boost-as-new-york-opens-applications-for-virtual-currency-exchange-licenses/) announced they are regulating Bitcoin.  And recently when the [IRS announced it is retroactively taxing Bitcoin as property](http://www.breitbart.com/Big-Government/2014/04/03/IRS-Retroactively-Taxes-Bitcoin), the community brought up the Dark Wallet system again.

I have bitcoin and do not plan on paying any tax on it.  I didn't "buy" any amount using taxable USD, and don't see a need for "regulation."  Trying to tax my Bitcoin is basically using a gun to say I need to pay a tax on the open-source software I write: it has value to a select few, and is completely useless to most. 

{% blockquote Hyper-Anonymising Bitcoin Service ‘Dark Wallet’ Launches Today http://www.coindesk.com/hyper-anonymising-bitcoin-service-dark-wallet-launches-today/ %}
{% img http://media.coindesk.com/2014/05/dark.jpg 'Bitcoin Dark Wallet' %}

The alpha version of Dark Wallet – the hyper-anonymising bitcoin wallet – goes live today.

Created by Amir Taaki and Cody Wilson, Dark Wallet provides new tools for financial privacy, including in-built coin mixing and ‘stealth’ wallet addresses.

As well as making it easier for people to disguise their transactions from the government, Dark Wallet is also a torpedo aimed at those in the bitcoin community who have embraced co-operation with regulators.

Conceived last year and partly funded by $50,000 raised on Indiegogo, the wallet was developed by unSystem, a collective of anarchist bitcoin developers. It represents an ethos that has been largely excluded from the mainstream discourse around bitcoin.

In January of this year, Circle CEO Jeremy Allaire told CoinDesk that bitcoin was “absolutely” moving away from its libertarian roots, and described attempts to avoid the regulation of bitcoin as playing into the hands of “anarchists and criminals”.
{% endblockquote %}

And I personally liked this little nugget from the related article...

{% blockquote Jeremy Allaire via CoinDesk http://www.coindesk.com/bitcoin-abandons-anti-establishment-wall-street/ %}
If your goals are to create a sort of shadow financial system that runs in offshore jurisdictions and is attractive for anarchists and criminals, then maybe [regulation] is not important.
{% endblockquote %}

The power to tax involves the power to destroy.

~E

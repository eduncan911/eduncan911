---
layout: post
title: "PLAY! A Video Game Symphony"
date: 2007-03-23 18:13:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Funny", "Games", "Videos"]
alias: ["/blog/A-Video-Game-Symphony.aspx", "/blog/a-video-game-symphony.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>Oh, what a flashback.&nbsp;Check the video below for Mario!</P>
<P>This is PLAY! A Video Game Symphony.&nbsp;&nbsp;And they will be in Fort Worth in Jan 2008.&nbsp; Here's the website:</P>
<P><A href="http://www.play-symphony.com/" mce_href="http://www.play-symphony.com/">http://www.play-symphony.com/</A></P>
<P mce_keep="true"><EMBED id=VideoPlayback style="WIDTH: 400px; HEIGHT: 326px" src=http://video.google.com/googleplayer.swf?docId=8169033819829939284&amp;hl=en type=application/x-shockwave-flash flashvars=""> </P>
<P mce_keep="true"><img alt='PLAY! A Video Game Symphony' src='http://www.play-symphony.com/about/about_pix/about_aroth.jpg'/></EMBED></P>

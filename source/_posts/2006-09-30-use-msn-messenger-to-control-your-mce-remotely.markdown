---
layout: post
title: "Use MSN Messenger to control your MCE remotely"
date: 2006-09-30 18:49:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Home Theater"]
alias: ["/blog/Use-MSN-Messenger-to-control-your-MCE-remotely.aspx", "/blog/use-msn-messenger-to-control-your-mce-remotely.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<p>This is an interesting idea for using an <a href="http://www.engadget.com/2006/09/28/record-greys-anatomy-hal-im-sorry-dave-im-afraid-i-can/" target="_blank">MSN Messenger bot to remotely control your MCE</a> machine.&nbsp; Like recording that show at the last second.</p><p>I thought this would be right up Ken&#39;s alley.</p><p>&nbsp;</p>

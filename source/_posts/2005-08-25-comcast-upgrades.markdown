---
layout: post
title: "Comcast upgrades"
date: 2005-08-25 04:11:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff"]
alias: ["/blog/comcast-upgrades.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>:::.. Download Stats ..:::<BR>Connection is:: 9119 Kbps about 9.1 Mbps (tested with 5983 kB)<BR>Download Speed is:: 1113 kB/s<BR>Tested From:: <A href="http://testmy.net/">http://testmy.net/</A> (server1)<BR>Test Time:: Thu Aug 25 09:33:08 CDT 2005 <BR>Bottom Line:: 163X faster than 56K 1MB download in 0.92 sec <BR>Diagnosis: Awesome! 20% + : 117.95 % faster than the average for host (comcast.net) <BR>Validation Link:: <A href="http://testmy.net/stats/id-DW4VGO9YK">http://testmy.net/stats/id-DW4VGO9YK</A> <BR><BR>Comcast upgraded Tennessee for standard accounts to have 6Mbps/384kbps and the Premium accounts to have 8Mbps/768kbps.</P>
<P>You know, I don't mind paying for Comcast Internet cause I get the first 6 months at $29.95.&nbsp; I'm also paying the extra $10/mo for the Premium service so that's $39.95.&nbsp; My cable tv is $29.95 as well for a year for their "Dish Upgrade" package, where you get digital channels, ondemand and hbo + $9.95 for the DVR.</P>
<P>I find myself using the upload of 768kbps much more cause of work and large file transfers.&nbsp; Like an 8h upload down to 4h the other night.</P>
<P>Like to see what happens when DSL jumps to 35+Mbps speeds sometime next year.&nbsp; While those speeds would be nice, it's the upload factor for me that must be fast.&nbsp; </P>

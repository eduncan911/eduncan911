---
layout: post
title: "Current &amp;amp;quot;Very Real&amp;amp;quot; eBay Scams - Western Union"
date: 2005-06-19 16:31:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff"]
alias: ["/blog/ebay-scams.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>This is a warning to all about a scan I almost fell into (cause man did I really want the product).</P>
<P>Lately I've been looking for a new laptop, and found the one I'd like to have for about $3800 *sign*.&nbsp; But behold, I found it on eBay for $800!&nbsp; Something must be wrong to be that cheap.&nbsp; And it seems everyday there are dozens of these floating around for $800.&nbsp; So I decided to contact one cause his info seemed complete:</P>
<UL>
<LI>The user had &gt; 200 positive feedbacks, so seemed creditable. 
<LI>The user had been a member for more then 5 years, wow. 
<LI>The user was in OH, claiming he had them in stock.</LI></UL>
<P>The things that tipped me off something was wrong was:</P>
<UL>
<LI>One&nbsp;day only auction (24h) 
<LI>Claiming I must email them to get the laptop for $800, and bluntantly providing their email right on the Auction. 
<LI>Very slight issues in description not quite matching (got to be a geek to spot it, but I did).</LI></UL>
<P>So I emailed them (from a spam email account, that I don't care about getting out), asking for some extremely specific questions.&nbsp; I get back a generic response:</P>
<P><FONT face="Courier New"><FONT size=2>"HI<BR>First I want to tell you that my unit is brand new, unopened box <BR>all&nbsp; accessories included and also an international warranty. <BR>The invoice will come at the same time with the package. <BR>My&nbsp; price is the best you could get: 800USD / unit including the <BR>shipping and insurance taxes. We will pay them because the package <BR>will be delivered from Europe. <BR>As delivery service we use UPS2days air service or overnight&nbsp; (with insurance and <BR>15 days return policy), because it's the faster. And if you will have <BR>a quick payment, we must also have a quick delivery. So that's why we <BR>use as a payment method Western Union money transfer, the fastest and <BR>also very secure way of sending money. <BR>So, if you agree with my terms I'm sure that we can close the deal as soon as possible. <BR>Waiting your quick answer right now,</FONT> "</FONT></P>
<P>Western Union and Europe, two major keywords are that big no no at eBay it seems (after reading several complaints on the Internet and on eBay's site).&nbsp; Here's a link to making sure you protect yourself from purchases:</P>
<P><A href="http://pages.ebay.com/help/confidence/isgw-buyer-tips.html">http://pages.ebay.com/help/confidence/isgw-buyer-tips.html</A></P>
<P>To report these jokers was no easy task, as it seems eBay is a bit hard to navigate.&nbsp; But I did find this link&nbsp;within it (about 1/2 way down), you can report them:</P>
<P><A href="http://pages.ebay.com/help/policies/rfe-spam-non-ebay-sale.html">http://pages.ebay.com/help/policies/rfe-spam-non-ebay-sale.html</A></P>
<P>Hope that helps you guys.</P>

---
layout: post
title: "XBOX 360's start to blog, by themselves!"
date: 2006-05-07 01:56:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Home Theater", "Funny"]
alias: ["/blog/XBOX-360_2700_s-start-to-blog_2C00_-by-themselves_2100_.aspx", "/blog/xbox-360_2700_s-start-to-blog_2c00_-by-themselves_2100_.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<p>Reading <a href="http://chrishammond.com/blogs/archive/2006/05/01/My_xbox360_blog.aspx">Christoc</a>&#39;s blog, I found out about that Xbox 360s are starting to come alive all by themselves and start blogging.</p><p>This is Chris&#39; Xbox crying out for help - <a href="http://www.360voice.com/blog.asp?tag=christoc">http://www.360voice.com/blog.asp?tag=christoc</a></p><p>I&#39;m going to setup our company Xboxes to do this at Telligent.&nbsp; Should make for a nice treat.</p>

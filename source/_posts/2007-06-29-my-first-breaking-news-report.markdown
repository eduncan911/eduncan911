---
layout: post
title: "My First Breaking News Report"
date: 2007-06-29 16:13:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Funny", "Police"]
alias: ["/blog/my-first-breaking-news-report.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>Breaking news!&nbsp; At 11:05 AM today, two suspects were apprehended for allegedly stealing a traffic pole that was to be erected at a corner near Elm and N. Good Latimer Expressway.&nbsp; I was first on the scene as it was kind of hard to ignore the police sirens and helicopter flying overhead.&nbsp; The suspects stopped right in front of my building, so it was quiet easy to snap these photos.&nbsp; With guns drawn, they ordered the male and female out of the truck.&nbsp; The rest is history obviously.&nbsp; </P>
<P>Lol.&nbsp; Thought I'd log my first breaking news post!&nbsp; Now, who can I sell these photos to?</P>
<P><IMG src="/blog/archives/images/MyFirstBreakingNewsReport-Kamawkad/1000.17.5866.640x427.IMG_0001.jpg.jpg" border=0 mce_src="/p/cache/1000.17.5866.640x427.IMG_0001.jpg.jpg"></P>
<P><IMG src="/blog/archives/images/MyFirstBreakingNewsReport-Kamawkad/1000.17.5867.640x427.IMG_0002.jpg.jpg" border=0 mce_src="/p/cache/1000.17.5867.640x427.IMG_0002.jpg.jpg"></P>
<P><IMG src="/blog/archives/images/MyFirstBreakingNewsReport-Kamawkad/1000.17.5868.640x427.IMG_0005.jpg.jpg" border=0 mce_src="/p/cache/1000.17.5868.640x427.IMG_0005.jpg.jpg"></P>
<P><IMG src="/blog/archives/images/MyFirstBreakingNewsReport-Kamawkad/1000.17.5869.640x427.IMG_0006.jpg.jpg" border=0 mce_src="/p/cache/1000.17.5869.640x427.IMG_0006.jpg.jpg"></P>

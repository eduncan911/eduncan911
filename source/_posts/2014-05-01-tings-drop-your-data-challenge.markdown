---
layout: post
title: "Ting's Drop your Data Challenge"
date: 2014-05-01 13:29:13 -0400
comments: true
categories: misc
tags: ["Ting", "Mobile"]
---
[Ting tries to bring your mobile data into focus](https://ting.com/blog/drop-your-data-challenge-get-ready/)

{% blockquote %}
{% img https://ting.com/wp-content/uploads/image_dropdata.png "Ting Drop Your Data" %}

On May 5 we’re launching a “Drop your Data” contest and challenging you to use as little mobile data as you possibly can. While we want you to think about reducing your data usage, we don’t mean using your smartphone less – there are many ways to conserve data without sacrificing your mobile phone experience. Take simple steps like connecting to Wi-Fi at home and at work, downloading your favorite audio and video podcasts automatically on Wi-Fi and so on.

The contest will be set up in two, week-long intervals and will utilize Onavo Count (Android/iPhone), a nifty data reporting app. The first week, you’ll use your smartphone as you normally would, and then for the second week, work on cutting down your mobile data usage! Open to customers and non-customers alike, all participants will be entered in a random draw to win $1,000 cash or one of three $150 Ting credits!

Don’t want to use Onavo Count? That’s fine. You can use the native data counting options on your phone or another counting method. Just ensure that the screenshots you submit clearly show the time period and your mobile data use (not Wi-Fi or Wi-Fi plus mobile data) for the time period.

If you’re not currently a Ting customer, you’ll also receive service credit for your percentage of data savings from the first week to the second. In other words, if you cut your mobile data use by 74%, you’ll receive a $74 Ting credit.

{% img https://ting.com/wp-content/uploads/datasaving.png "save mobile data" %}

Stay tuned for complete details inside this blog post on Monday, May 5.
{% endblockquote %}

Even though I've carefully been monitoring my data usage for years because I value battery life
over "always connected" distractions, I'll be entering this contest to see if I can drop it even
further.  Challenge accepted.

I've been a Ting user for nearly 1 year now, have saved over $1000 (which already paid for our new phones,
canceling our contracts, and then some) and cannot live without the real-time stats and alerts they
have.  

I like how they invite people that are not Ting users to enter the contest as well.

~E
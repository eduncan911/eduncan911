---
layout: post
title: "GoogleSearch MODs"
date: 2005-08-08 14:00:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Computer Programming"]
alias: ["/blog/googlesearch-mods.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>I&nbsp;stumbled across&nbsp;a blog&nbsp;post the other day where someone modified the GoogleSearch and GoogleIndex binaries to get additional files indexed.&nbsp; </P>
<P>Larry Gadea has wrapped those changes, and more, into some nice easy plugins to the Google Desktop Search.&nbsp; <A href="http://www.trivex.net/">http://www.trivex.net/</A></P>
<P>Also, for those unaware Google has opened most of their downloaded features to the public:</P>
<P><A href="http://code.google.com/">http://code.google.com/</A></P>
<P>&nbsp;</P>

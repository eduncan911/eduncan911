---
layout: post
title: "WindowsXP Service Pack 2 (SP2)"
date: 2005-05-21 20:26:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff"]
alias: ["/blog/windowsxp-service-pack-2-sp2.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>This is another reason Mac users stick to the Macs.&nbsp; It's 'cause of crap like these Toolbars and Firewall add-on applications that annoying the hell out of you/takes away from the User Expierence.&nbsp; Get rid of them all!&nbsp; Go back to the way Windows was originally thought of, simple Windows that allows you to get your work done.</P>
<P>I know I know.&nbsp; This is an old topic.&nbsp; Hey, my blog is online now.&nbsp; So I'm going to rant.&nbsp; :)</P>
<P>Some might still be frighten and may not have this installed.&nbsp; Install it.&nbsp; </P>
<P>It doesn't hinder me, as an extreme poweruser.&nbsp; I program in many different langauges and tools (from Visual Studio .NET, to Dreamweaver, to breaking out VS6 occassionally for old software).&nbsp; Not to mention Java and graphics/video editting.&nbsp; No problems here.</P>
<P>The benifits far out-weigh any large download or whatnots.</P>
<P><STRONG>Built-In PopUp Blocker</STRONG><BR>Yes, that's right.&nbsp; You no longer need those resource hogging Toolbars!&nbsp; Use the built-in features of SP2.</P>
<P><STRONG>Excellent Kernel-Level Firewall (and yes, it blocks OUTGOING too!)</STRONG><BR>Some people are dedicated ZoneAlarm gurus (with paid subscriptions).&nbsp; Some have been frightened into not trusting their LinkSys router with firewall cause of the extreme hackers that still take them down (this is true, but only if you are targetted by someone specifically).</P>
<P>There's no need for any of this, period.&nbsp; Now don't get this SP2 Firewall confused with the firewall in the standard XP and the one with SP1.&nbsp; This one is COMPLETELY differently, now embedded into the system's kernel (if you don't know what the kernel is, just rest assured that it's at the ground level now instead of just an application level before).</P>
<P>Yes, it blocks outgoing as well.&nbsp; And it even notifies you that it has blocked an application with a nice little baloon popup.&nbsp; :)</P>
<P>The last ZoneAlarm Pro I debugged (had to fix) annoyed the hell out of me!&nbsp; All of those warnings, huge popup windows, etc.&nbsp; Why someone would use that, especially complete-handicapped people I have no idea.&nbsp; It only adds to the extreme confusion of the Internet.</P>
<P>Yes, ZoneAlarm keeps stats but there's no need for that crap.&nbsp; It's useless information (not to mention annoying).&nbsp; Doesn't matter if you were attacked 10 or 10,000 times.&nbsp; Compromised servers out there on the net will continue to hit your machine.&nbsp; </P>

---
layout: post
title: "Buy KickStarter at MoMA"
description: "NYC's Museum of Modern Art adds a collection of KickStarter products to its online store."
image: http://eduncan911.com/blog/images/buy-kickstarter-online-at-moma.png
video: 
date: 2014-05-14 16:25:06 -0400
comments: true
categories: ["lifestyle"]
tags: ["KickStarter", "Technology"]
---

[Popular KickStarter projects are available online at the MoMA](http://www.momastore.org/museum/moma/CategoryDisplay_10451_10001_26708_56161_-1_Y_Kickstarter%20at%20MoMA?ref=MoMAxKS)

{% img /blog/images/buy-kickstarter-online-at-moma.png "Buy Kickerstart on the MoMA" %}

{% blockquote %}
In honor of NYCxDESIGN—New York City's official citywide celebration of design—the MoMA Design Store is pleased to present a suite of products brought to life by Kickstarter. By involving the public in the creative process, Kickstarter uses the power of community to help designers take great ideas from concept to reality. MoMA Design Store is proud to honor these individuals and their designs as examples of how everyone is capable of making incredible things.
{% endblockquote %}

This came across in my inbox today and immediately jumped in.

3D Doodler, come to me!  The book lamp and recyclable USB sticks, 4x 8 GB, are also on my list.


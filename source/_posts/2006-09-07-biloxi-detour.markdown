---
layout: post
title: "Biloxi Detour"
date: 2006-09-07 14:09:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Travel", "Biloxi", "Mississippi"]
alias: ["/blog/Biloxi-Detour.aspx", "/blog/biloxi-detour.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<p><img src="/blog/archives/images/biloxi_detour_hurricane_katrina_bridge.jpg" alt="Biloxi Detour Hurricane Katrina Bridge" /></p><p>On Hwy US 90 that runs along the coast of the Gulf, the <em>Biloxi Bay Bridge</em> is still out from Katrina over a year ago.</p><p>I&#39;ll admit, I crossed way past the &quot;no drive&quot; zone to get this shot.&nbsp; But hey, the car barely screeched through the no-drive barriers.&nbsp; It&#39;s a MINI after all.</p><p>Along&nbsp;the detour, I also ran into some fisherman getting ready to put to sea.</p><p><img src="/blog/archives/images/biloxi_detour_hurricane_katrina_shipyard.jpg" /></p>

---
layout: post
title: "Best Cloud Computing and Security Talk EVAR"
description: James Mickens talks distributed computing in a memorial way.
image: http://eduncan911.com/blog/images/james-mickens-best-dist-computing-talk.jpg
video: http://vimeo.com/95066828
date: 2014-05-14 03:13:47 -0400
comments: true
categories: [software]
tags: ["Cloud Computing", "Security", "Distributed Systems"]
---

[James Mickens on distributed computing at PDX 2014](http://vimeo.com/95066828)

{% vimeo 95066828 %}

This showed up in my twitter feed; so, I gave it 30m of my life and I am glad I did.

{% hattip Xander Sherry https://twitter.com/xandersherry %}
---
layout: post
title: "HowTo: Move a File to another FileGallery"
date: 2006-09-22 23:04:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Computer Programming", "Community Server"]
alias: ["/blog/move-file-to-filegallery.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<div align="left"><span class="366342421-22092006"><font face="Arial" size="2">I&#39;m going to start posting a lot of my old Duncan Nuggets (as <a href="http://dbvt.com/blog/" target="_blank"><font color="#666666">Dave Burke</font></a> likes to call them) here for others to start reading.&nbsp; This is the first of a lot&nbsp;to come.<br /><br /></font></span></div><div align="left"><span class="366342421-22092006"></span></div><div align="left"><span class="366342421-22092006"></span></div><div align="left"><span class="366342421-22092006"></span></div><div align="left"><span class="366342421-22092006"><font size="4">How to move a file from one FileGallery to another</font></span><span class="366342421-22092006"><font face="Arial" size="2">&nbsp;<br /></font></span></div><div align="left"><span class="366342421-22092006"><font face="Arial" size="2">NOTE: This script was written for Community Server 2.1 RTM.&nbsp;<br /><br /></font></span></div><div align="left"><span class="366342421-22092006"></span></div><div align="left"><span class="366342421-22092006"><font face="Arial" size="2"></font></span></div><div align="left"><span class="366342421-22092006"><font face="Arial" size="2"><span class="366342421-22092006"><font face="Arial" size="2">This method uses a stored procedure to move a single file (aka Post) to another File Gallery (aka Section).&nbsp;&nbsp;</font></span>Attached&nbsp;is an SQL script file named <strong>move_file_to_filegallery.zip</strong>&nbsp;that when executed, will create a stored procedure named </font></span><span class="366342421-22092006"><font face="Arial" size="2"><strong>_move_file_to_filegallery</strong> in your database.&nbsp; <br /><br /></font></span></div><div align="left"><span class="366342421-22092006"></span></div><div align="left"><span class="366342421-22092006"><font face="Arial" size="2"></font></span></div><div align="left"><span class="366342421-22092006"><font face="Arial" size="2">After the sproc is created, you can execute it by running the following command:<br /><br /></font></span></div><div align="left"><span class="366342421-22092006"></span></div><div align="left"><span class="366342421-22092006"><font face="Arial" size="2"></font></span></div><div align="left"><span class="366342421-22092006"><font face="Courier New" size="2"><font color="#ff0000">EXEC _move_file_to_filegallery @PostID, @toSectionID, @ClearTags<br /><br /></font></font></span></div><div align="left"><span class="366342421-22092006"><font color="#ff0000" face="Courier New"></font></span></div><div align="left"><span class="366342421-22092006"><font face="Arial" size="2"></font></span></div><div align="left"><span class="366342421-22092006"><font face="Arial" size="2"></font></span></div><div align="left"><span class="366342421-22092006"><font face="Arial" size="2">You need two bits of information beforing executing:</font></span></div><ul><li><div align="left"><span class="366342421-22092006"><font face="Arial" size="2"><strong>@PostID</strong> - This is the PostID of the file you want to move.&nbsp; </font></span></div><ul><li><div align="left"><span class="366342421-22092006"><font face="Arial" size="2">You can find this&nbsp;in the URL&nbsp;by viewing the file in CS&#39; normal File view.&nbsp; For example, if we look up the SDK for CS 2.1 the link is: <a href="http://communityserver.org/files/folders/communityserver/entry543125.aspx"><font color="#000000">http://communityserver.org/files/folders/communityserver/entry<strong>543125</strong>.aspx</font></a>.&nbsp; So the PostID of this file is <strong><font size="3">543125</font></strong>.</font></span></div></li></ul></li><li><div align="left"><span class="366342421-22092006"><font face="Arial" size="2"><strong>@toSectionID</strong> - This is the SectionID of the File Gallery you want to move the file to.</font></span></div><ul><li><div align="left"><span class="366342421-22092006"><font face="Arial" size="2">You can find this in the URL by going into the Control Panel, Files, and selecting that File Gallery to manage.&nbsp; For example, the URL for managing the CommunityServer Current Releases folder is: <a href="http://communityserver.org/controlpanel/files/default.aspx?sectionid=234"><font color="#000000">http://communityserver.org/controlpanel/files/default.aspx?sectionid=<strong>234</strong></font></a>.&nbsp; So this SectionID that I want to move the file to is <strong><font size="3">234</font></strong>.</font></span></div></li></ul></li></ul><div align="left"><span class="366342421-22092006"><font face="Arial" size="2">Following the examples above now that we have the two required integers, we can now execute the sproc to move the file.&nbsp; The line would read:<br /><br /></font></span></div><div align="left"><span class="366342421-22092006"></span></div><div align="left"><span class="366342421-22092006"><font face="Arial" size="2"></font></span></div><div align="left"><span class="366342421-22092006"><font size="2"><span class="366342421-22092006"><font face="Courier New" size="2"><font color="#ff0000">EXEC _move_file_to_filegallery @PostID = 234, @toSectionID = 543125, @ClearTags = 1<br /><br /></font></font></span></font></span><span class="366342421-22092006"><font face="Arial" size="2"><span class="366342421-22092006">NOTE: It is recommended to always clear the Tags, since they only exist in the previous FileGallery and not the new one.&nbsp; So leave it @ClearTags&nbsp;= 1.</span></font></span></div><div align="left"><span class="366342421-22092006"><font face="Arial" size="2"><span class="366342421-22092006"></span></font></span></div><div align="left"><span class="366342421-22092006"><font face="Arial" size="2"><span class="366342421-22092006"></span></font></span></div><div align="left"><span class="366342421-22092006"><font face="Arial" size="2"></font></span></div><div align="left"><span class="366342421-22092006"><font face="Arial" size="2">There is also error checking built in, so view the Messages for any errors that may have occurred.<br /><br /></font></span></div>

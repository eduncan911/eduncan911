---
layout: post
title: "Mad Passing @ Deal's Gap"
date: 2006-09-11 23:36:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Automotive", "Travel", "Tennessee"]
alias: ["/blog/Mad-Passing-_4000_-Deal_2700_s-Gap.aspx", "/blog/mad-passing-_4000_-deal_2700_s-gap.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<p>As I start the process of encoding the rest of my footage of Deal&#39;s Gap, I went out to see what other videos were out there.</p><p><a href="http://video.google.com/videoplay?docid=-4987615321238909941&amp;q=MAD+PASSING+gap&amp;hl=en" target="_blank">Here&#39;s a video called Mad Passing at Deal&#39;s Gap</a>, as it&#39;s exactly that.&nbsp; </p><p>Why post this?&nbsp; Because it makes me feel better about posting my videos.&nbsp; It&#39;s gotten to the point that in my next release, I will post a disclaimer about how I could safely see far enough ahead to make the turn.&nbsp;&nbsp;The videos only show a telescoped image, not the entire view of our viewpoint while driving.</p><p>&nbsp;</p><p>&nbsp;</p>

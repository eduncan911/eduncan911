---
layout: post
title: "Jim Allchin launches WindowsVistaBlog.com"
date: 2006-10-26 16:38:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Community Server", "Telligent"]
alias: ["/blog/jim-allchin-launches-windowsvistablog-com.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<p>The Windows Vista Team launched a makeover&nbsp;to their blog site,&nbsp;now using Community Server.&nbsp; They&nbsp;partnered with <a href="http://weblogs.asp.net/rhoward/archive/2006/10/26/Jim-Allchin-launches-WindowsVistaBlog.com.aspx" target="_blank">Telligent</a> to&nbsp;overhaul the the Windows Vista Team Blog, and I&#39;m quite impressed with the layout.&nbsp;Jim Allchin talks about their updated&nbsp;launch <a href="http://windowsvistablog.com/blogs/windowsvista/archive/2006/10/25/welcome-to-our-new-blog.aspx" target="_blank">here</a>.&nbsp;</p><blockquote><p><a href="http://windowsvistablog.com/" title="Windows Vista Blog" target="_blank">WindowsVistaBlog.com</a></p></blockquote><p>Using some nice transparent PNGs (and some IE6 hacks to get around the non-PNG support), it&#39;s a very clean site.</p><p>Like the IEBlog I&#39;ve started to read, from the Internet Explorer team, I&#39;ve really liked the direction that companies are taking by&nbsp;opening up their engineers and development teams to the public eye.&nbsp; Letting you talk one on one with them.&nbsp; Microsoft and <a href="http://www.direct2dell.com/one2one/archive/2006/07/11/117.aspx" title="Dell&#39;s Direct2Dell Announcement" target="_blank">Dell</a> are the two leading ones that I know of at this time.</p><p>So, go add Windows Vista Blog to your RSS reader and stay up to date on it&#39;s latest news.</p><p><img alt='Jim Allchin launches WindowsVistaBlog.com' src='http://windowsvistablog.com/users/avatar.aspx?userid=11870'/></p>

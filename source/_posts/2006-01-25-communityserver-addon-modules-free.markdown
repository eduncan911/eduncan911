---
layout: post
title: "CommunityServer AddOn Modules - Free!"
date: 2006-01-25 14:59:00 -0500
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Computer Programming"]
alias: ["/blog/communityserver-addon-modules-free.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>This past weekend, Telligent held a contest with internal developers to see who could generate the best CSModule.&nbsp; There were about 20 submissions - none from me.&nbsp; I was too busy working all weekend.&nbsp; Next time though, I'll win the Gold.&nbsp; :)</P>
<P><A href="http://developer.communityserver.org/default.aspx/CS.FirstCSModuleContent">http://developer.communityserver.org/default.aspx/CS.FirstCSModuleContent</A></P>
<P>You can access the files and source code here:</P>
<P><A href="http://communityserver.org/files/40/csmodules/entry511330.aspx">http://communityserver.org/files/40/csmodules/entry511330.aspx</A></P>
<P>This is also a great example of how easy it is to write your own CSModules by following these examples.</P>
<P>&nbsp;</P>

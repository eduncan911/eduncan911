---
layout: post
title: "A blast from the past - Nintendo R.O.B."
date: 2006-09-26 03:21:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Games"]
alias: ["/blog/A-blast-from-the-past-_2D00_-Nintendo-R.O.B_2E00_.aspx", "/blog/a-blast-from-the-past-_2d00_-nintendo-r.o.b_2e00_.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<p><img src="/blog/archives/images/nintendo_ROB.jpg" alt="Nintendo R.O.B." /></p><p>I can&#39;t believe it.&nbsp; When I saw a picture of the Nintendo ROB, it instantly brought back fond memories from&nbsp;my&nbsp;Nintendo era (1985),&nbsp;oh so long forgotten.&nbsp; The game was to get one of the two disks spinning by having ROB pick it up, turn to his far left, place it on a spinner to spin up, and then placing it on the Red or Blue button - depending on which one needed to be hit.&nbsp;</p><p>Appearently this guy recently got his Lunix&nbsp;box to talk to it, making the news today.</p><p><a href="http://www.engadget.com/2006/09/25/control-the-nes-r-o-b-with-your-pc/" target="_blank">Sourced form engadget</a>.</p>

---
layout: post
title: "Win a XBOX 360 Every 10 minutes"
date: 2005-08-28 15:04:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Friends and Family"]
alias: ["/blog/win-a-xbox-360-every-10-minutes.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>In case you haven't heard Mountain Dew and XBOX have teamed up to give away&nbsp;a free XBOX 360 every 10 minutes, 24 hours a day, 7 days a week for 30 days straight starting, well, today.&nbsp; </P>
<P><A href="http://www.every10minutes.com">www.every10minutes.com</A>&nbsp;(website seems a bit slow with all of the traffic of launch)</P>
<P>Now I try to stay away from caffeine, but I think I found a reason to get hyper for the next month.</P>
<P>&nbsp;</P>

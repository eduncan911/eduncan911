---
layout: post
title: "Gmail and Sourcing your Spam"
date: 2005-06-17 20:44:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Personal", "Computer Programming"]
alias: ["/blog/Gmail-Aliases.aspx", "/blog/gmail-aliases.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>Gmail has a neat feature of allowing you to enter aliases with your email address.&nbsp; This is a nice trick to help find the source of why you are now getting upteen thousand spam emails in your GMail account.</P>
<P>For exmaple, my email address is in the form of (this is a fake email, to keep spam bots from stealing my email address): <A href="mailto:eric@gmail.com" mce_href="mailto:eric@gmail.com">eric@gmail.com</A></P>
<P>To make an alias, just add the plus (+) symbol to your username followed by whatever you want to type.&nbsp; Like: <A href="mailto:eric+forums@gmail.com" mce_href="mailto:eric+forums@gmail.com">eric+forums@gmail.com</A></P>
<P>That's it.&nbsp; There is no setup to do, it works automatically.&nbsp; Try it.&nbsp; Send yourself an email to <A href="mailto:myaccount+EricRules@gmail.com">myaccount+EricRules@gmail.com</A>.&nbsp; It will drop right into your Gmail inbox.</P>
<P>I do this a lot for contest or new websites that I have to signup on in order to read content.&nbsp; I've already caught a few sites that I got spam from.&nbsp; </P>
<P>What pisses me off is a lot of PHP websites do not support this "plus" symbol in the email address.&nbsp; Saying it's an invalid email address, or worse just parsing it out!&nbsp; Come on system admins, fix your shit!&nbsp; </P>
<P>(To the Admins) RFC2822, RFC2821, RFC1123, and RFC822 are what you can follow to ensure correct email address formats.&nbsp; Which, yes, includes the "+" symbol!&nbsp; Fix your sites, please.</P>
<P mce_keep="true">&nbsp;</P>

---
layout: post
title: "Toolbars no more! (continued)"
date: 2005-05-21 20:46:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff"]
alias: ["/blog/toolbars-no-more-continued.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>Now what prompted me to write up <A href="/blogs/eduncan911/archive/2005/05/21/get_rid_of_those_toolbars.aspx">why you don't need Toolbar applications any longer </A>was because of the&nbsp;mess of Included Software now for applications you normally download.&nbsp; From reputable&nbsp;sources!</P>
<P>The RealOne player tries to sneak you into installing Yahoo!'s Toolbar on their homepage.&nbsp; Even after that, it STILL tries to install Yahoo!'s Toolbar on installation.&nbsp; </P>
<P>The QuickTime installer tries to sneak in the Yahoo! bar.&nbsp; As well as the Adobe Reader 7.0.&nbsp; Windows Media Player tries to sneak in the MSN Toolbar.&nbsp; And another freeway app tries to sneak in Google's (from a machine I was setting up today).</P>
<P>Don't you just want to SLAP the bastards?&nbsp; I feel very sorry for end-users that aren't catching this.&nbsp; They'll have all sorts of Toolbars installed.</P>
<P>I understand each company is trying to push some advertising/steer users to their websites (Google, MSN, or Yahoo!)&nbsp;to generate additional income.&nbsp; But guys, WE ARE FIGURING IT OUT!&nbsp; They are getting as bad as SpyWare crap now, bogging down our machines.</P>
<P>&nbsp;</P>

---
layout: post
title: "Host your videos on Google!"
date: 2005-06-28 14:42:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Automotive", "Geek Stuff", "Friends and Family", "Games"]
alias: ["/blog/videos-on-google.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>*** UPDATE 8/9/2005 - Google launched this service about a month ago and I've been serving videos since.&nbsp; It's VERY cool, but requires you to download a browser plugin.</P>
<P>Google is about to launch a new Video service.&nbsp; From what I gather, it will do two things.&nbsp; </P>
<P>The first is unlimited storage/hosting of your video!&nbsp; I've uploaded about 890MB and they haven't even batted an eye.&nbsp; No longer will you have to upload your video to a server somewhere to be hosted 'cause Google seems to be solving that issue (i.e. home videos?).</P>
<P>The other thing is indexing META data for searching.&nbsp;&nbsp;For Google's Videos, this information are things such as Title, Description, Language, Production company, Genre, dates, credits, etc.&nbsp; It also allows someone to upload a Transcript of the video!&nbsp; All of this information is indexed in Google's massive search library.&nbsp; </P>
<P>It also allows you to charge for access.&nbsp; I'm not sure how that will work, but it's there.</P>
<P>Here's the link that will be soon heard around the world: </P>
<P><A href="http://video.google.com/">http://video.google.com/</A></P>
<P>If you need a Gmail account, let me know as I have quite a few invitations left.</P>
<P>&nbsp;</P>

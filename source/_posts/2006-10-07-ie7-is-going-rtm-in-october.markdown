---
layout: post
title: "IE7 is going RTM in October"
date: 2006-10-07 18:43:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Community Server"]
alias: ["/blog/IE7-is-going-RTM-in-October.aspx", "/blog/ie7-is-going-rtm-in-october.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<p><img alt='IE7 is going RTM in October' src='http://msdn.microsoft.com/nodehomes/graphics/headlines/55x55_IE_addons.gif'/>What&#39;s interesting here is the team will be forcing IE7 installs out through normal Automatic Updates - overwriting your IE6 overnight.</p><p>That&#39;s both impressive, and a bit scary at the same time.</p><p>You can read more about the IE Team&#39;s launch <a href="http://blogs.msdn.com/ie/archive/2006/10/06/IE7-Is-Coming-This-Month_2E002E002E00_Are-you-Ready_3F00_.aspx" target="_blank">here at their blog</a>.</p><p>I for one am all for this!&nbsp;&nbsp; I&#39;ve been using IE7 since Beta 2 (beta 1 was aweful buggy), and now been designing most client projects at Telligent under IE7 (which renders fine under FF).&nbsp; But IE6 has had some issues.&nbsp; </p><p>So I for one welcome this update.</p>

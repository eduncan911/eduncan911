---
layout: post
title: "Microsoft Error Reporting - Share your pain with MS!"
date: 2006-07-14 13:07:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Funny"]
alias: ["/blog/Microsoft-Error-Reporting-_2D00_-Share-your-pain-with-MS_2100_.aspx", "/blog/microsoft-error-reporting-_2d00_-share-your-pain-with-ms_2100_.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>Sourced from: <A href="http://ettf.net/archives/2330">http://ettf.net/archives/2330</A></P>
<P>You know those "A system error has occurred.&nbsp; Send this error to Microsoft" dialog boxes that pop up when an application/service crashes?</P>
<P>Well, here's proof that they actually help Microsoft employees - in the most direct method possible.&nbsp; lol</P>
<P align=center>
<OBJECT height=350 width=425><PARAM NAME="movie" VALUE="http://www.youtube.com/v/ry7u6JF_B1c">
<embed src="http://www.youtube.com/v/ry7u6JF_B1c" type="application/x-shockwave-flash" width="425" height="350"></embed></OBJECT></P>

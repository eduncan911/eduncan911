---
layout: post
title: "BlueTooth - Making life too simple for everyone"
date: 2005-11-14 23:56:00 -0500
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Friends and Family", "Personal"]
alias: ["/blog/bluetooth-making-life-too-simple-for-everyone.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>Let me put this into perspective.&nbsp; </P>
<P>You can:</P>
<UL>
<LI>Answer cell calls with&nbsp;a BlueTooth headset. 
<LI>Answer office phone calls with&nbsp;the same&nbsp;BlueTooth headset. 
<LI>Talk in&nbsp;PC Video Conferences with the same BlueTooth headset. 
<LI>Talk with VOIP for things such as Gamers and Vonage, using the same BlueTooth headset. 
<LI>With a laptop, connect to the Internet via 256+Kbps DUN anywhere that has SprintPCS coverage (i.e. on road trips or no WiFi hotspots, &lt; 2s to connect). 
<LI>Newer Sprint phones are&nbsp;suppose to support up to 1.2Mbps download rate (like the one mentioned below). 
<LI>Especially with Sprints Nights @ 7pm, it's <STRONG><U><EM>free</EM></U></STRONG> all night&nbsp;and weekend long.</LI></UL>
<P>With three devices:</P>
<UL>
<LI><A href="http://www1.sprintpcs.com/media/Assets/ueContent/phones/whoa-phoneDetailsSPHA940LSS.gif" target=_blank>Sprint MM-A940 2.0 Megapixel cell phone </A>(or any BlueTooth cell, just sprint provides faster data xfer) 
<LI><A href="http://www.headsets.com/headsets/wireless/plantronics/510s/description.html" target=_blank>Plantronics Voyager 510S BlueTooth System for Desktop </A>(DON'T BUY FROM THAT LINK!&nbsp; Search the net, much cheaper versions).&nbsp; If you don't want the Office Phone functionallity, get just the headset for $60) 
<LI>BlueTooth-enabled Laptop supporting DUN and&nbsp;Audio/Voice BlueTooth profiles (or a $50 USB adapter if it doesn't)</LI></UL>
<P>And you can do this&nbsp;<EM><STRONG>without removing the cell phone from your belt or pocket</STRONG></EM>, or lifting your handset on your office phone.&nbsp; That's just too cool.&nbsp; Just imagine, riding along on a road trip or in the car pool to work in the morning.&nbsp; Flip open the laptop and connect to the Internet, without removing the phone from your belt clip.</P>
<P>I've spent a long time researching these devices, and finally the day has come.&nbsp; They are on order.&nbsp; </P>
<P>Now if I can just get them all to work...&nbsp; </P>
<P><BR>&nbsp;</P>

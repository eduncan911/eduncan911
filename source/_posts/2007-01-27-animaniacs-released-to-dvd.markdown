---
layout: post
title: "Animaniacs released to DVD!"
date: 2007-01-27 20:43:00 -0500
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Personal", "Funny", "Videos", "Animaniacs"]
alias: ["/blog/Animaniacs-released-to-DVD.aspx", "/blog/animaniacs-released-to-dvd.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>For those of you looking for&nbsp;that special gift&nbsp;to get&nbsp;me, the Animaniacs DVD collection is it.&nbsp; I just read where in 2007, Warner Bros. is releasing the first 50 episodes in a DVD set (there's 99 episodes total).&nbsp; Thank you WB!&nbsp; Not sure if it is out yet, but keep an eye out.<BR>[video]<BR>I've wondered for a long long time what happened to this&nbsp;blockbuster Steven Spielberg series.&nbsp; Think calling this a blockbuster is going a bit too far?&nbsp; Not so.&nbsp; Per the updated <A class="" href="http://en.wikipedia.org/wiki/Animaniacs" target=_blank mce_href="http://en.wikipedia.org/wiki/Animaniacs">Wikipedia entry</A>:</P>
<BLOCKQUOTE>
<P><EM>Animation fans consider Animaniacs the high point of the Warner Bros. revival of the 1990s that was inspired by the original </EM><A title="Termite Terrace" href="http://en.wikipedia.org/wiki/Termite_Terrace"><EM>Termite Terrace</EM></A><EM>. </EM></P>
<P><EM>Animaniacs premiered on </EM><A title="September 13" href="http://en.wikipedia.org/wiki/September_13"><EM>September 13</EM></A><EM>, </EM><A title=1993 href="http://en.wikipedia.org/wiki/1993"><EM>1993</EM></A><EM>. New episodes of the show were aired during the 1993 through 1998 seasons, and episodes were rerun in </EM><A title="TV syndication" href="http://en.wikipedia.org/wiki/TV_syndication"><EM>syndication</EM></A><EM> for several years after production of new episodes ceased. One feature-length </EM><A title=Direct-to-video href="http://en.wikipedia.org/wiki/Direct-to-video"><EM>direct-to-video</EM></A><EM> Animaniacs movie, </EM><A title="Wakko's Wish" href="http://en.wikipedia.org/wiki/Wakko%27s_Wish"><EM>Wakko's Wish</EM></A><EM>, was released on VHS only (there has not yet been a </EM><A title=DVD href="http://en.wikipedia.org/wiki/DVD"><EM>DVD</EM></A><EM> release) in 1999. The series was popular enough for Warner Bros. Animation to invest in additional episodes of the show past the traditional 65-episode marker for syndication; a total of 99 episodes were finally produced. One theatrical cartoon short film starring the Warner siblings, "I'm Mad," was produced and released to theaters in 1994 with the feature </EM><A title="Thumbelina (film)" href="http://en.wikipedia.org/wiki/Thumbelina_%28film%29"><EM>Thumbelina</EM></A><EM>.</EM></P>
<P><EM>The show introduced the popular cartoon characters </EM><A title="Pinky and the Brain" href="http://en.wikipedia.org/wiki/Pinky_and_the_Brain"><EM>Pinky and the Brain</EM></A><EM>, who were subsequently </EM><A title="Spin off" href="http://en.wikipedia.org/wiki/Spin_off"><EM>spun-off</EM></A><EM> into their own TV series in 1995.</EM></P></BLOCKQUOTE>
<P>I've been a long-time fan of this series and have many sound-bites of the series.&nbsp; Such as Yacko's Multiplication skit (grab a piece of paper and follow).&nbsp;</P>
<P><A href="/files/folders/comedy/entry3948.aspx">/files/folders/comedy/entry3948.aspx</A></P>
<P>It wasn't so a kids cartoon, but&nbsp;targeting more of the young-adults to early 20 year-olds.&nbsp; I was 17 when it first aired, and watched it <SPAN style="FONT-SIZE: 10pt; FONT-FAMILY: Arial; mso-fareast-font-family: 'Times New Roman'; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA">religiously</SPAN> until I got bogged down in college.&nbsp; After that, I kinda of lost track of them - catching an episode here and there.&nbsp; And then, they were pulled from syndication in early 2000.&nbsp; Gone.</P>
<P><img alt='Animaniacs released to DVD!' src='/blog/archives/images/storage/1000.4.3950.Animaniacs.jpg'/></P>

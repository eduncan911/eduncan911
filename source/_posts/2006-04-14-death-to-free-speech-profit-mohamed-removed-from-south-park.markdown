---
layout: post
title: "Death to Free Speech: Prophet Mohamed removed from South Park"
date: 2006-04-14 19:17:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Personal", "Funny", "Southpark"]
alias: ["/blog/Death-to-Free-Speech_3A00_-Profit-Mohamed-removed-from-South-Park.aspx", "/blog/death-to-free-speech_3a00_-profit-mohamed-removed-from-south-park.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P><IMG style="WIDTH: 200px; HEIGHT: 110px" border=0 align=left src="/blog/archives/images/storage/1000.4.788.1003_bigwheels7_m3.jpg" width=200 height=110>Comedy Central joins an ever growing list of TV and Newspapers&nbsp;censoring the image of the Prophet Mohamed.</P>
<P>Last week South Park&nbsp;started a two-part episode spoofing the Family Guy sitcom (<A href="/funsies/southpark-_2f00_-profit-mohamed_3f00_.html" mce_href="/blogs/eduncan911/archive/2006/04/06/571.aspx">read previous post</A>)&nbsp;titled <EM>First Amendment</EM>.&nbsp; It was making fun of Fox refusing to show the Prophet.&nbsp; Within the first episode, the SouthPark creators dared Comedy Central to allow SP to&nbsp;allow the Prophet Mohamed to be shown in Episode Two this week.&nbsp; I'm sure there was a lot of talk around the conference rooms there.</P>
<P>In Episode two, aired Wednesday at 10 PM EST, they attempted to show the Prophet.&nbsp; But they cut the footage, saying <EM>"Comedy Central has refused to show the Prophet"</EM>.&nbsp; But yet, they still had the "Voice" on there.&nbsp; </P>
<P>South Park has crossed a number of ethnic and racial lines, but there's always a moral the creators try to convey which seems to get lost by most (of not all) anti-South Park people.</P>
<P>The one for this episode was that if we are forced to use censorship, then the terrorist have won.&nbsp; As where does censorship stop?&nbsp; Free Speech entitles oneself a free voice.&nbsp; Given Comedy Central is FCC monitored, the FCC has not banned the image of Prophet Mohamed.&nbsp; It was Comedy Central itself that refused&nbsp;to show it (scarred?).</P>
<BLOCKQUOTE style="MARGIN-RIGHT: 0px" dir=ltr>
<P>Eric Cartman: <EM><STRONG>"If you hate a TV show, all you have to do is to get an episode pulled.&nbsp; Pretty soon the show is compromised and it goes off the air. ...so my plan is to use this whole Mohamed thing as a way to scare the network into pulling tonight's show [of Family Guy].</STRONG></EM></P>
<P><EM><STRONG>I'm going to use fear to get them to do what&nbsp;I want."</STRONG></EM></P>
<P>Bart Simpson's spoofed character: <EM><STRONG>"Isn't that like Terrorism?"</STRONG></EM></P>
<P>Eric Cartman: <STRONG><EM>"No.&nbsp; It isn't like Terrorism.&nbsp; It</EM>&nbsp;IS&nbsp;<EM>Terrorism."</EM></STRONG></P></BLOCKQUOTE>
<P>We all are given the right to Free Speech by the constitution of the United States of America.&nbsp; Giving-in to Terrorist threatening violence is the same as giving into demands when they kidnap hostages: it lets the terrorist know they can get their way.</P>
<P>Would I censor image from my website if you posted&nbsp;the cartoon&nbsp;in a comment?&nbsp; No.&nbsp; But I myself respect other religions and beliefs: which I know showing the image would be greatly disrespectful.&nbsp; If&nbsp;I wanted to thwart the powers-at-be and go against the “man” to make a crude statement (like I would have in my teens), I would have posted it here.</P>
<P>I have been a big South Park fan since episode one due to their true-to-Earth viewpoints, cunning storylines and willing to say the things we are thinking, on the tube – daring all others.<SPAN>&nbsp; </SPAN>And I will remain a South Park fan.</P>
<P>People I have met that are against South Park tell me what offended them.<SPAN>&nbsp; </SPAN>When talking to these people, out of the 10 seasons (yes, 10!), all of these complaints and boycotts of South Park all have one thing in common: The focus on the graphic, grotesque nature of the cartoons and speech.<SPAN>&nbsp; </SPAN></P>
<P>If you walk up to me and say, <EM>“Eric, I can’t believe you are a South Park fan.”</EM>&nbsp; Then I would state the obvious, <EM>“You didn't like the movie Fight Club, Secret Window, and other movies with an alternative plot behind the obvious storyline,&nbsp;did you?”</EM><SPAN>&nbsp; </SPAN>South Park on the surface can look highly offensive (to those offended).<SPAN>&nbsp; </SPAN>But there’s always an underlining moral if you look close enough.<SPAN>&nbsp; </SPAN>Over the last two weeks, it was Censorship and how the Prophet Mohamed cartoon issue has&nbsp;taken away&nbsp;our right to Free Speech in America.&nbsp; Or at least, scarred network tv into censorship.</P>
<P>So I say no to censorship.&nbsp; Show the image if you wish.<SPAN>&nbsp; </SPAN>And I will not judge you any differently.&nbsp; </P>
<P><img alt='Death to Free Speech: Prophet Mohamed removed from South Park' src='/photos/geek_stuff/images/788/original.aspx'/></P>

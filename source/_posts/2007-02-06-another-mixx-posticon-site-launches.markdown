---
layout: post
title: "Another PostIcon Site Launches - Soapinions"
date: 2007-02-06 18:33:00 -0500
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Community Server", "PostIcon", "The MiXX Collection"]
alias: ["/blog/another-mixx-posticon-site-launches.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<p>&nbsp;<img style="width: 200px; height: 251px;" align="left" src="http://i119.photobucket.com/albums/o158/soapinions/CBS%20Soaps/eileen.jpg" width="200" height="251" mce_src="http://i119.photobucket.com/albums/o158/soapinions/CBS%20Soaps/eileen.jpg"></p>
<p>Barbie, here's a website for you.&nbsp; </p>
<p>Soapinions is a community site for&nbsp;discussion with the latest soap operas on daytime television.&nbsp; Like what the Sex of Chad's Lover is!&nbsp; OMG!&nbsp; It can't be!</p>
<p>They were looking for more of a "visual" appeal.&nbsp; Well, they got now.&nbsp; :)</p>
<p><a href="http://soapinions.com/blogs/default.aspx" target="_blank" mce_href="http://soapinions.com/blogs/default.aspx">http://soapinions.com/blogs/default.aspx</a></p>
<p mce_keep="true">Using my MiXX.PostIcon, they are simply going back and flooding their original posts with new images.&nbsp; I kind of actually like the larger 100px sizes vs my 63px here on my site.&nbsp; I may update mine.&nbsp; </p>

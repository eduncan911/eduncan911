---
layout: post
title: "PostIcon Updated"
date: 2006-12-16 21:38:00 -0500
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Computer Programming", "Community Server", "PostIcon", "The MiXX Collection"]
alias: ["/blog/posticon-updated.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>You can download&nbsp;<EM><STRONG>The MiXX Collection</STRONG></EM>&nbsp;that includes the updated PostIcon AddOn from these urls:</P>
<P>(install only)<BR><A href="/files/folders/2611/download.aspx">/files/folders/2611/download.aspx</A><A href="/Downloads/MiXXCollection-CS30-v1.0.zip"></A></P>
<P>(source code-must compile)<BR><A href="/files/folders/5894/download.aspx">/files/folders/5894/download.aspx</A></P>
<P><STRONG>Change Log: <BR></STRONG><STRONG>2006-12-16 (v0.9.3.25764)</STRONG><BR>&nbsp;- Changed from throwing exceptions to just logging exceptions, to <BR>&nbsp;&nbsp; allow the rest of the page/site to load without redirecting to an<BR>&nbsp;&nbsp; error.&nbsp; To debug, look in your CSExceptions for UnknownError and<BR>&nbsp;&nbsp; FileNotFound errors logged.&nbsp; <BR>&nbsp;- Added in parsing of the regex groups in the config file. This<BR>&nbsp;&nbsp; allows for custom regex patterns to be used, and you can<BR>&nbsp;&nbsp; explicitly set what group this module will parse the needed values<BR>&nbsp;&nbsp; for.<BR>&nbsp;- Fixed a small issue to where the PostIconModule was using the last<BR>&nbsp;&nbsp; used AnchorPosition specified in a post as the default for the <BR>&nbsp;&nbsp; time the AppPool was running.</P>
<P><A class="" href="/archive/2006/12/11/mixx-posticon-released-to-the-public.aspx" mce_href="/archive/2006/12/11/mixx-posticon-released-to-the-public.aspx">Updated previous version</A>, and deleted old files.</P>
<P><img alt='PostIcon Updated' src='http://images.google.com/images?q=tbn:M2zAIOKQ2-_JMM:http://growabrain.typepad.com/photos/uncategorized/comedian_george_carlin.jpg'/></P>

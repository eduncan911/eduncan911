---
layout: post
title: "Upgrading projects to Visual Studio 2005 RTM from Beta2"
date: 2005-11-10 23:01:00 -0500
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Computer Programming"]
alias: ["/blog/upgrading-projects-to-visual-studio-2005-rtm-from-beta2.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>I've been upgrading theSpoke.net to the .NET Framework 2.0 using Visual Studio 2005 RTM lately and there were a number of methods and properties that were removed (broke the build).&nbsp; </P>
<P>Asking around, I ran across the API differences between Beta2 and RTM.&nbsp; I found it extremely useful.</P>
<P>Here's the file: <A href="http://download.microsoft.com/download/E/C/E/ECE1E64C-824D-4905-AE1C-FD13DDC78BC8/Beta2RTMAPI.msi">Beta2RTMAPI.msi (256 KB)</A></P>
<P>It unpacks a few Excel files easily displaying what has been removed and added between the two.</P>
<P>&nbsp;</P>

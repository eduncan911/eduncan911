---
layout: post
title: "Google Desktop (again)"
date: 2006-04-06 00:15:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff"]
alias: ["/blog/Google-Desktop-_2800_again_2900_.aspx", "/blog/google-desktop-_2800_again_2900_.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>It's been a while since I installed this gem.&nbsp; I had it on my previous desktop, but haven't gotten around to installing on my laptop yet.<?xml:namespace prefix = o ns = "urn:schemas-microsoft-com:office:office" /><o:p></o:p></P>
<P>I've been using the inadequate built-in Find for searching for my emails on the Exchange and remote IMAP and local Personal Folder files.<o:p></o:p></P>
<P>Yesterday, I got fed up when I couldn't find an email for work.&nbsp; So when I got home late, I installed the Google Desktop, fired up Outlook and connected to the Exchange server, and let the laptop site on all night - Indexing.<o:p></o:p></P>
<P>I woke up and before hopping into the shower, decided to test my newly installed Google desktop to see if I could find that one email.<o:p></o:p></P>
<P>Within the first search, the 2nd result was what I was looking for.<o:p></o:p></P>
<P class=MsoNormal style="MARGIN: 0in 0in 0pt"><o:p>&nbsp;I may try MSN Desktop Search next.</o:p></P>
<P class=MsoNormal style="MARGIN: 0in 0in 0pt"><o:p></o:p>&nbsp;</P>

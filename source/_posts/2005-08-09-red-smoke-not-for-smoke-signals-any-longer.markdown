---
layout: post
title: "Red smoke not for Smoke Signals any longer"
date: 2005-08-09 13:23:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Automotive"]
alias: ["/blog/red-smoke-not-for-smoke-signals-any-longer.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<p><img align="left" height="133" src="http://www.americanlemans.com/NEWS/NEWSIMAGES/5920058593193_IMAGE2_thumb.JPG" width="200" />Now I have nothing against drifting.&nbsp; If I had a car to do it, I would.&nbsp; I&#39;ve read the magazine articles where muscle-car gurus step into some of the top Drifting competition cars and come out saying they have a whole new respect for the sport.&nbsp; But I guess until I save up enough cash to waste a set of tires in 10m, I guess I will not be a competitor.</p><p>My drifting experience was in my 1975 Monte Carlo (big big nose, short short rear end, driver&#39;s position waay back near rear axle).&nbsp; That car was the perfect drifter, just I was 17 at the time (that was 13 years ago).&nbsp; I got a few lectures from the cops back then for my &quot;drifting&quot; around corners.&nbsp; For some reason, that car was so easy to slam down into 1st, mash the throttle and whip the tail end out.&nbsp; I was taking hair-pin turns at ~30 to 35mph.&nbsp; Given I was using both lanes sliding sideways.&nbsp; &quot;It just looks so cool&quot;, my friends said.&nbsp; Yes, peer pressure.&nbsp; </p><p>So if any of my highschool buddies are reading this, remember the red-sled?&nbsp; lol</p><p>&nbsp;</p><p>&nbsp;</p>

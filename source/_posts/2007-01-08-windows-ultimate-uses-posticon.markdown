---
layout: post
title: "Microsoft's Windows Ultimate launches with MiXX PostIcon!"
date: 2007-01-08 19:27:00 -0500
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Community Server", "Telligent", "PostIcon", "The MiXX Collection"]
alias: ["/blog/windows-ultimate-uses-posticon.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P><IMG style="WIDTH: 73px; HEIGHT: 73px" height=73 src="http://windowsultimate.com/blogs/PostIcons/erikneuenschwander/16.jpg" width=73 mce_src="http://windowsultimate.com/blogs/PostIcons/erikneuenschwander/16.jpg">&nbsp;<FONT size=7>+</FONT> <IMG style="WIDTH: 73px; HEIGHT: 73px" height=73 src="http://windowsultimate.com/blogs/PostIcons/alexkipman/23.jpg" width=73 mce_src="http://windowsultimate.com/blogs/PostIcons/alexkipman/23.jpg">&nbsp;<FONT size=7>+</FONT>&nbsp;<IMG style="WIDTH: 73px; HEIGHT: 73px" height=73 src="http://windowsultimate.com/blogs/PostIcons/alexkipman/22.jpg" width=73 mce_src="http://windowsultimate.com/blogs/PostIcons/alexkipman/22.jpg">&nbsp;<FONT size=7>=</FONT> <IMG style="WIDTH: 73px; HEIGHT: 73px" height=73 src="http://windowsultimate.com/blogs/PostIcons/alexkipman/20.jpg" width=73 mce_src="http://windowsultimate.com/blogs/PostIcons/alexkipman/20.jpg"></P>
<P>Telligent announces another great site we've done for Microsoft -&nbsp;Windows Ultimate Edition.</P>
<P><A class="" href="http://windowsultimate.com/" target=_blank mce_href="http://windowsultimate.com/">http://windowsultimate.com/</A></P>
<P>What's even cooler about this site?&nbsp; It uses my&nbsp;<A class="" href="/archive/tags/The+MiXX+Collection/default.aspx" mce_href="/archive/tags/The+MiXX+Collection/default.aspx">The MiXX Collection's PostIcon</A> package (woot!).&nbsp; <A class="" href="http://blog.danbartels.com/archive/2007/01/09/windowsultimate-com-launched.aspx?CommentPosted=true#commentmessage" target=_blank mce_href="http://blog.danbartels.com/archive/2007/01/09/windowsultimate-com-launched.aspx?CommentPosted=true#commentmessage">Dan Bartels</A> was the developer for that portion of the project, and he slightly tweaked the code to display the post's Attachment, if it was an image, as the PostIcon as well as the normal logic.</P>
<P>I will roll this into the next release of the PostIcon module.&nbsp; Most likely between the check for the BBCode and inline image parsing (an intermediate step).&nbsp; Thoughts?</P>
<P><img alt='Microsoft's Windows Ultimate launches with MiXX PostIcon!' src='http://windowsultimate.com/blogs/PostIcons/erikneuenschwander/16.jpg'/></P>

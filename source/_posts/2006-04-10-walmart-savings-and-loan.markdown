---
layout: post
title: "Walmart Savings and Loan"
date: 2006-04-10 17:50:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Financial"]
alias: ["/blog/Walmart-Savings-and-Loan.aspx", "/blog/walmart-savings-and-loan.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>The&nbsp;FDIC is currently reviewing Walmart's application to get into the Banking business.</P>
<P>The want to, "Save money by getting around the costs of Credit Card processes by creating their own."</P>
<P>They said they don't want to open a Retail, like a Bank of Walmart.&nbsp; They just want to get around all of the transaction fees credit cards charge with going through banks.</P>
<P>Some people testifying in favor of Walmart is the Salvation Army.</P>
<P>More info (and full story) is at <STRONG><A href="http://www.npr.org/templates/story/story.php?storyId=5334287">NPR</A></STRONG></P>
<P>&nbsp;</P>

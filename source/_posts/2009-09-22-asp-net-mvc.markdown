---
layout: post
title: "ASP.NET MVC"
date: 2009-09-22 17:02:13 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Computer Programming", "Asp.Net Mvc", "Type Safety"]
alias: ["/blog/asp-net-mvc.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<p><img style="border-right-width: 0px; margin: 0px 0px 0px 20px; display: inline; border-top-width: 0px; border-bottom-width: 0px; border-left-width: 0px" title="funny-pictures-cat-does-not-like-puppies" border="0" alt="funny-pictures-cat-does-not-like-puppies" align="right" src="/blog/archives/images/ASP.NETMVC_C748/funnypicturescatdoesnotlikepuppies.jpg" width="364" height="484" /> I am starting a new series that will be focusing on ASP.NET MVC design patterns.&#160; What I will be focusing on is enterprise-ready patterns and practices that scales across cloud computing, coding with type safety, and abstractions of WebForms-like server controls – using the MVC pattern.&#160;&#160; <a href="http://blog.codeville.net/" target="_blank">Steve Sanders</a> and <a href="http://haacked.com/" target="_blank">Phil Haack</a> have about a year head start on everything-mvc, so I highly recommend reading up on their blogs for the basics and advanced solutions.&#160; </p>  <p>I will keep updating this post with each post for future references.&#160; So, bookmark it or <a href="/archive/tags/asp.net+mvc/default.aspx" target="_blank">you can subscribe to the series here</a>.</p>  <ul>   <li><a href="/blog/type-safety-with-asp-net-mvc-futures.aspx">Type Safety with ASP.NET MVC Futures</a> </li>    <li><a href="/blog/html-renderaction-for-asp-net-mvc-1-0.aspx">Html.RenderAction&lt;TController&gt; for ASP.NET MVC 1.0</a></li> </ul>  <p>&#160;</p>  <p>You can also follow me on Twitter via eduncan911, if you can weed out my random thoughts from these postings.</p> <img alt='ASP.NET MVC' src='/blog/archives/images/ASP.NETMVC_C748/funnypicturescatdoesnotlikepuppies.jpg'/>   

---
layout: post
title: "Is Microsoft still building their own CPU?"
date: 2014-04-30 13:40:08 -0400
comments: true
categories: ["technology"]
---
I was going through some old posts during my import and found where I mentioned that 
[Microsoft was to build their own CPUs](/blog/archives/microsoft-to-build-its-own-cpus.html) 
for the next-gen Xbox after the 360 - nearly 8 years ago (yes, the 
[Xbox 360 is almost 9 years old](http://en.wikipedia.org/wiki/Xbox_360)).

{% blockquote Tomshardware http://www.tomshardware.com/reviews/microsoft-xbox-one-console-review,3681-3.html Xbox One Review: Unifying Your Living Room Experience %}
The Xbox One's CPU component brings Microsoft’s architecture of choice back to x86, just like the first-generation Xbox. This time, the company kicked IBM to the curb and opted not to jump into another Intel/Nvidia love triangle. Instead, it put a ring on AMD's finger, tapping the same source for general-purpose and graphics computing IP. At the same time, Sony was courting AMD's semi-custom division as well. Both competitors ended up sharing a bed, but due to NDAs was never caught laying down.
{% endblockquote %}

Microsoft had Intel for Xbox 1st gen.  Then IBM for the 2nd gen.  For the 3rd gen Xbox One and PS4, we 
all know [they went with AMD now](http://www.anandtech.com/show/6976/amds-jaguar-architecture-the-cpu-powering-xbox-one-playstation-4-kabini-temash).

So this got me thinking... [Chuck Thacker is still at Microsoft](http://research.microsoft.com/en-us/people/cthacker/).  It does take a while to design and produce your CPU.  I wonder if Microsoft will drop that bomb in the next decade.

It's sad because after the Zune fiasco, with Zune software no longer compatible with 
the Zune players, I'm left with $600 in media players that are worthless and useless 
cause I can't sync any music to them.  So I don't think I'll ever go back to 
Microsoft products after that.  It was one too many low blows over the last decade.

In related news, a buddy of mine sums up how the Xbox One vs PS4 has played out over the last 6 months:

<blockquote class="twitter-tweet" lang="en"><p>Every time I go to look at new Xbox One releases I regret not buying a PS4 instead.</p>&mdash; Jason Alexander (@jasona) <a href="https://twitter.com/jasona/statuses/460530041789480960">April 27, 2014</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

~E
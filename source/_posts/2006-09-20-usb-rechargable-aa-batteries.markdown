---
layout: post
title: "USB Rechargable AA batteries!"
date: 2006-09-20 15:16:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff"]
alias: ["/blog/USB-Rechargable-AA-batteries_2100_.aspx", "/blog/usb-rechargable-aa-batteries_2100_.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<p>In&nbsp;continuing my RSS feeds of <a href="/controlpanel/blogs/www.engadget.com" target="_blank">Engadget</a> (almost too much to stay on top of!), some very cool stuff pops up on here.</p><p>This has to be one of the best ideas for USB in a long time.&nbsp; <a href="http://www.engadget.com/2006/09/19/usbcell-batteries-feature-built-in-usb-plug/" target="_blank">USB rechargable AA batteries</a>!</p><p><img height="200" src="http://www.blogsmithmedia.com/www.engadget.com/media/2006/09/usbcellaa.jpg" style="width: 200px; height: 200px" width="200" /></p>

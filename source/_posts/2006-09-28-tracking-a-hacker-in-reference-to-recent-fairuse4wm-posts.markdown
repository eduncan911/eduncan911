---
layout: post
title: "Tracking a hacker (in reference to recent FairUse4WM posts)"
date: 2006-09-28 03:39:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Computer Programming"]
alias: ["/blog/Tracking-a-hacker-_2800_in-reference-to-recent-FairUse4WM-posts_2900_.aspx", "/blog/tracking-a-hacker-_2800_in-reference-to-recent-fairuse4wm-posts_2900_.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P><IMG height=240 src="http://www.blogsmithmedia.com/www.engadget.com/media/2006/09/unknown_portrait.jpg" width=220 align=left mce_src="http://www.blogsmithmedia.com/www.engadget.com/media/2006/09/unknown_portrait.jpg">During the recent <A href="http://www.engadget.com/2006/09/26/microsoft-sues-viodenta-for-copyright-infringement/" target=_blank mce_href="http://www.engadget.com/2006/09/26/microsoft-sues-viodenta-for-copyright-infringement/">FairUse4WM lawsuits that Microsoft</A> is heading up recently, I've started to learn more and more about the<A href="http://engadget.com/tag/PlaysForSure" target=_blank mce_href="http://engadget.com/tag/PlaysForSure">PlaysForSure</A> DRM answer that Microsoft has created to try to satisfy the DMCA bitches (think BMI, Universal - anything DVD of CD Audio related).</P>
<P>What I found interesting is the age-old techniques I've used to track down hackers (and prosecute them in court, via companies that have hired me in the past) has become so commonly understood these days that they explain it in their recent lawsuit against <A href="http://engadget.com/tag/viodentia" target=_blank mce_href="http://engadget.com/tag/viodentia">Viodentia</A> - and people understand exactly the process.</P>
<P>See, back in the day (late 80s, early 90s) no one had a clue how to track down a hacker.&nbsp; I made some side cash tracking down these guys so long ago cause no one knew the technology enough.&nbsp; Now someone makes a post, and it amazes me how common-knowledge it has become.</P>
<P>Now don't get me wrong.&nbsp; I've done my fair-share of cracks and hacks, and knew enough about re-writing the tcp stack to hide my tracks.&nbsp; So don't get pissed if it was me that testified against you way back then.&nbsp; Did I do it&nbsp;against the underground pirate groups?&nbsp; No.&nbsp; It was for those guys that would hack into computers of friends of mine to do nothing more then destroy their networks.</P>
<P>Aren't the best Security Experts the best hackers?&nbsp; Was always my sales pitch in the&nbsp; past for such jobs.&nbsp; :)</P>
<P>Err, off the point.&nbsp; <A href="http://www.engadget.com/2006/09/26/microsoft-sues-viodenta-for-copyright-infringement/" target=_blank mce_href="http://www.engadget.com/2006/09/26/microsoft-sues-viodenta-for-copyright-infringement/">The post above</A> clearly outlines one of the most common methods for tracking a hacker.</P>
<P>ISPs will not talk to you unless you have a court order.&nbsp; So create a suit (as MS did above), then talk to the ISPs.&nbsp; They will gladly hand over any and all records.&nbsp; Which most of the time you can track to a single IP and MAC address at that time of the email/file submission/port connectivity/logs/etc.&nbsp; MACs on broadband are linked to a physical&nbsp;address on file for that user that is being billed, which will be handed over via the ISP.&nbsp; Either have the feds ask for the warrant, or contact the local courts for the area to obtain one - and have the local PD/fbi unit conduct the search/arrest.</P>
<P>Rest is normal process.&nbsp; </P>
<P>There are dozens of way to get around the above, that most hackers don't even attempt.&nbsp; Like accessing a computer&nbsp;has been hacked, then a remote hacker would piggy back everything they do off of that machine.&nbsp; That's just one of many ways.</P>
<P>Now in this case, <A href="http://engadget.com/tag/viodentia" target=_blank mce_href="http://engadget.com/tag/viodentia">Viodentia</A> claims to live outside of the United States.&nbsp; That's good and bad, depending on his country.&nbsp; This adds an extra (and extra thick) layer of law process, as you now must go through federal courts to obtain an extradition.&nbsp; Now this I have never been involved with, so I'll leave that part up to the lawyers.</P>

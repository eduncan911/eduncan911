---
layout: post
title: "Black and Blue"
date: 2005-10-07 16:13:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Friends and Family", "Personal"]
alias: ["/blog/black-and-blue.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>Last Saturday while playing coed flag-football for a sports league here in&nbsp;Nashville I felt a sharp pain in my left hamstring.&nbsp; I tend to have&nbsp;a high threshold for a little pain, so I kept on playing nursing my left leg (jumping off of my right now).&nbsp; Well,&nbsp;after a&nbsp;complicated reverse to get a tackle caused another such pain to go through my right hamstring.</P>
<P>Thinking I could stretch them out, I sidelined myself and stretched a bit.&nbsp; Was a bit sore, but got back into the game.&nbsp; </P>
<P>We won 41 to 0 (a shutout).&nbsp; </P>
<P>Limping around we took the other team out for a few beers to calm the soreness.&nbsp; I got home, but was still in pain.&nbsp; I laid around resting for the night.&nbsp; Sunday I awoke to some horrible pain.&nbsp; "This can't be good", I thought.&nbsp; Limped in to see&nbsp;the PTs at the YMCA Monday and they assure me it's nothing torn, just a sever pull in both hamstrings.</P>
<P>Today after the current round of heat-n-stretching excersises, I noticed a little bruising-like colors on the back of one leg.&nbsp; After I was done witht he stretching I examed them in the mirror.&nbsp; OUCH.&nbsp; It's like someone took a baseball bat to the back of my legs!&nbsp; </P>
<P>I don't think I'll be playing this Saturday.&nbsp;<IMG src="/emoticons/emotion-9.gif"></P>

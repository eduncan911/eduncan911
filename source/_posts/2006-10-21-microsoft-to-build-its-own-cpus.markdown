---
layout: post
title: "Microsoft to build its own CPUs"
date: 2006-10-21 17:51:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Games"]
alias: ["/blog/microsoft-to-build-its-own-cpus.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<p><img align="left" height="305" src="http://www.blogsmithmedia.com/www.engadget.com/media/2006/10/thacker.jpg" style="width: 225px; height: 305px" width="225" />Microsoft tried Intel in the <a href="http://en.wikipedia.org/wiki/Xbox" target="_blank">Xbox Gen 1</a>.&nbsp; Microsoft tried IBM in the <a href="http://en.wikipedia.org/wiki/Xbox_360" target="_blank">Xbox 360</a>.</p><p>Now Microsoft has decided to just make their own microprocessor for the next generation Xbox.&nbsp; As quoted from <a href="http://www.engadget.com/2006/10/21/microsoft-to-start-its-own-chip-design-lab-for-new-xbox/" target="_blank">Engadget</a>:</p><blockquote><p>&quot;According to a report in yesterday&#39;s Gray Lady, Microsoft will be starting its own chip design division, with the ultra-creative name: &quot;The Computer Architecture Group,&quot; and will be split between Redmond, Washington and Mountain View, California. The venerable NYT adds that Microsoft will use the lab to beef up chips in the next-generation Xbox (the Xbox 720?), and will be headed by Charles P. Thacker. He&#39;s formerly of the legendary Xerox Palo Alto Research Center, where he helped work on the original Alto and the invention of Ethernet.&quot;</p><p><img alt='Microsoft to build its own CPUs' src='http://www.blogsmithmedia.com/www.engadget.com/media/2006/10/thacker.jpg'/></p></blockquote>

---
layout: post
title: "Thirty posts in thirty days"
date: 2009-09-20 03:07:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Personal"]
alias: ["/blog/thirty-posts-in-thirty-days.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P><A href="/blog/archives/images/Thirtypostsinthirtydays_14530/cartoon.jpg" mce_href="/blog/archives/images/Thirtypostsinthirtydays_14530/cartoon.jpg"><IMG style="BORDER-RIGHT-WIDTH: 0px; MARGIN: 0px 20px 0px 0px; DISPLAY: inline; BORDER-TOP-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px" title=cartoon border=0 alt=cartoon align=left src="/blog/archives/images/Thirtypostsinthirtydays_14530/cartoon_thumb.jpg" width=179 height=203 mce_src="/blog/archives/images/Thirtypostsinthirtydays_14530/cartoon_thumb.jpg"></A> Happy one year anniversary!&nbsp; As <A href="http://christoc.com/" mce_href="http://christoc.com">Chris Hammond</A> pointed out this week, it has been exactly one year since I last made a post to my blog – this blog!</P>
<P>It is not for a lack of content.&nbsp; I have well over 100 of “should have, will do, todos, blog this” of bookmarks, code snippets, and more.&nbsp; It didn’t ring a bell until christoc actually pinged me this week to tell me.&nbsp; </P>
<P>So, today I start a new goal: Thirty posts in thirty days.&nbsp; Now, there may be several posts in one day in succession, and other empty days.&nbsp; But, there will be at least 30.&nbsp; :)</P>

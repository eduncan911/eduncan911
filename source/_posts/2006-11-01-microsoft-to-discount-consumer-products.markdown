---
layout: post
title: "Microsoft to discount consumer products"
date: 2006-11-01 02:22:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff"]
alias: ["/blog/microsoft-to-discount-consumer-products.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<p>&nbsp;<a href="http://windowsvistablog.com/photos/blog_photo_gallery/picture479306.aspx" title="Windows Vista Blog" target="_blank"><img alt="Office Home and Student 2007" height="425" src="http://windowsvistablog.com/photos/blog_photo_gallery/images/479306/386x425.aspx" style="width: 386px; height: 425px" title="Office Home and Student 2007" width="386" /></a></p><p>While checking out the new (and&nbsp;big shock to the design) packing that Vista&nbsp;and Office 2007 are going to have,&nbsp;I couldn&#39;t help but noticing on the <a href="http://windowsvistablog.com/blogs/windowsvista/archive/2006/10/30/announcing-new-packaging-for-windows-vista-and-2007-office-system.aspx" title="Open Windows Vista Blog" target="_blank">Windows Vista Blog</a> there&#39;s a new version coming out: Office Home and Student 2007.</p><p>Does this mean that Microsoft is finally giving us home users a discount in pricing to match the Student Edition pricing?&nbsp; No more &quot;have to be a student to get it at this price&quot;?</p><p>That would be very nice.&nbsp; Nice indeed!</p><p>&nbsp;</p>

---
layout: post
title: "IE Developer Toolbar"
date: 2005-09-21 14:36:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Computer Programming"]
alias: ["/blog/ie-developer-toolbar.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>Xander gave us a heads-up about a new Developer&nbsp;Toolbar for Internet Explorer.&nbsp; People have been using a similar toolbar in FireFox for developing webpages (which I never used, so can't say how nice it was).</P>
<P>Well, over the last week I've been using this developer toolbar and it's awesome!</P>
<P>Here's the MS link: <A href="http://www.microsoft.com/downloads/details.aspx?FamilyID=e59c3964-672d-4511-bb3e-2d5e1db91038&amp;displaylang=en">http://www.microsoft.com/downloads/details.aspx?FamilyID=e59c3964-672d-4511-bb3e-2d5e1db91038&amp;displaylang=en</A></P>
<P>Enjoy!</P>

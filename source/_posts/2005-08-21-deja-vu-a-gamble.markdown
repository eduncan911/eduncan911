---
layout: post
title: "Deja Vu, A Gamble"
date: 2005-08-21 21:02:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Personal"]
alias: ["/blog/deja-vu-a-gamble.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P class=MsoNormal style="MARGIN: 0in 0in 0pt">I really think this phenomenon is annoying.&nbsp; Time and time again, I see something that I could swear I saw before.&nbsp; A car, a movie, a place I could swear I've been to before, a book that I haven't read but seem to remember bits.&nbsp; <BR><BR>I want to have words with the person who planted the seed in my head that, "Deja Vu is simply where one side of your brain processes something faster then the other side.&nbsp; So you have a feeling that you've been there before, or seen that person before."&nbsp; I think it was in a movie and I want to speak to that writer.&nbsp; <BR><BR>Today, it was another one of those "I've seen that person before".&nbsp; But this time, it was both a woman and a guy that looked familiar (didn't hurt that she was very attractive for me to notice).<BR><BR>So I sat here in Panera, looking over the notebook playing eye-swap with her across the room.&nbsp; "I know this woman from somewhere.&nbsp; That guy with her looks familiar too.&nbsp; Do I say something, do I leave it?&nbsp; They haven't recognized me, but I think I know them.&nbsp; Do I just leave it cause I could be wrong?&nbsp; Do I say something in case I am right?&nbsp; Screw it, I do know them."<BR><BR>So I walk over, "Hi.&nbsp; I feel I've met you guys somewhere before.&nbsp; Was it at <?xml:namespace prefix = st1 ns = "urn:schemas-microsoft-com:office:smarttags" /><st1:place w:st="on"><st1:City w:st="on">Carlton</st1:City></st1:place>’s birthday party?"<BR><BR>"We just moved here yesterday from CA and have never been in TN before until our flight landed yesterday.&nbsp; Sorry.", she says.<BR><BR>Feel free to donate to the "idiot" fund...<BR style="mso-special-character: line-break"></P>

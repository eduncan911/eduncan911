---
layout: post
title: "Holding out for what you want"
date: 2006-08-21 00:44:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Personal"]
alias: ["/blog/Holding-out-for-what-you-want.aspx", "/blog/holding-out-for-what-you-want.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<p><img src="/blog/archives/images/holding-out-for-what-you-want.jpg" /></p><p>Just had to blog about waiting for something you want, but don&#39;t need.</p><p>Over the years I&#39;ve used my friend&#39;s Canon Digital Rebel cameras.&nbsp; This is quite an impressive camera.&nbsp; I&#39;ve used the earlier one, the updated one, etc.&nbsp; And the 8 MP XT has been out for a little while.</p><p>Well I never had a good camera - or even a camera at all.&nbsp; I did buy a used digital&nbsp;one from a friend of mine that was ok (couldn&#39;t take night or fast-motion shots), but it stopped working reliably shortly after that.</p><p>Next week I&#39;ll be taking a long road trip, and I didn&#39;t want to miss out on anything.&nbsp; So I did it.&nbsp; I&nbsp;dipped into the funds, and purchased a good SLR - finally.</p><p>A&nbsp;new&nbsp;Canon Digital Rebel XT 8MP - for $579!&nbsp; I think the eBayer thought it was the older 6.3 MP version.</p><p>Anyhoot, first picture under the &quot;custom&quot; settings is above.&nbsp; And many more to come!</p><p>&nbsp;</p>

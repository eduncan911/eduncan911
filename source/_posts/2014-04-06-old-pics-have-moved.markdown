---
layout: post
title: "Old Pics have Moved"
date: 2014-04-06 21:54:32 -0400
comments: false
categories: misc
---
If you are reading this, then I get to say this:

{% blockquote %}
I moved my blog, and the photos didn't go with it.
{% endblockquote %}

You can try browsing the images in [my Flickr set called Blogs](https://www.flickr.com/photos/eduncan911/sets/72157600808141363/) 
for the one you were looking for; but alas, I didn't upload all photos (sorry Venna White fans).

Fret not.  If you really need that photo, just use the contact button at in the header and/or footer
of this site and send me the link you want the photo(s) for.

~E

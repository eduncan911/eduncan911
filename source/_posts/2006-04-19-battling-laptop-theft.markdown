---
layout: post
title: "Battling Laptop Theft"
date: 2006-04-19 17:06:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Friends and Family"]
alias: ["/blog/Battling-Laptop-Theft.aspx", "/blog/battling-laptop-theft.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>My friend Todd Major sent me this cute video.</P>
<P><A href="http://news.com.com/1606-2-6060109.html">http://news.com.com/1606-2-6060109.html</A></P>
<P>No, can't do video blogging of it here because it is flash.&nbsp; So you'll have to click on it there.</P>
<P>Now all you need is a LoJack.</P>
<P>&nbsp;</P>

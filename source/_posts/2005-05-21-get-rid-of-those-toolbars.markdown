---
layout: post
title: "Toolbars no more!"
date: 2005-05-21 20:22:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff"]
alias: ["/blog/get-rid-of-those-toolbars.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>The Yahoo!'s Toolbar.&nbsp; The&nbsp;Google's Toolbar.&nbsp; The&nbsp;MSN Toolbar.&nbsp; The "I'm a company, use our toolbar" Toolbar.</P>
<P>These were cool for about 15 minutes, as it allowed quick searches for the search engine you prefer to use.&nbsp; They also were very nifty by having the ability to block PopUp windows.</P>
<P>My current viewpoint of these?&nbsp; GET RID OF THEM!!!&nbsp; </P>
<P>There is no need for them any longer.&nbsp; Zip, nada, none.&nbsp; All they do is use system resources (memory, delay your browse from opening faster, etc).&nbsp; Yet adding to the confusion and messing up the User Experience.&nbsp; </P>
<P>Now some may ask, "Wait, I want the popup blocker!!"&nbsp; You already have a PopUp blocker built INTO Internet Explorer (<A HREF="/blogs/eduncan911/archive/2005/05/21/150.aspx" target=_blank>once you install Service Pack 2</A>).</P>
<P>Ok, what about quick searches you may ask?&nbsp; Easy, install a Microsoft-now-distributed PowerToy called TweakUI (<A href="http://www.microsoft.com/windowsxp/downloads/powertoys/xppowertoys.mspx">http://www.microsoft.com/windowsxp/downloads/powertoys/xppowertoys.mspx</A>).&nbsp; Once installed, open it and browse to <STRONG>Internet Explorer</STRONG>, <STRONG>Search</STRONG>.</P>
<P>Here are the built-in keyword shortcuts for searching.&nbsp; Google isn't installed by default, so I add it by clicking Create, enter the letter "g", and use this 'special' URL:</P>
<P><A href="http://www.google.com/search?q=%s">http://www.google.com/search?q=%s</A></P>
<P>Notice the %s.&nbsp; This is described in the Create window.&nbsp; You can use this format for Yahoo, MSN (even though MSN is built-in).</P>
<P>Now, when I want to search Google, I open a browser (which you have to do anyhow for the Toolbar app if you had one), and in the URL type:</P>
<P>g Braves Baseball&lt;press enter&gt;</P>
<P>BAM, I now have Google results for Braves Baseball.&nbsp; :)</P>
<P>&nbsp;</P>

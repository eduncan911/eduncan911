---
layout: post
title: "Dell gets down One2One with you"
date: 2006-07-09 14:23:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Community Server"]
alias: ["/blog/Dell-gets-down-One2One-with-you.aspx", "/blog/dell-gets-down-one2one-with-you.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>Dell and Telligent&nbsp;has recently launched a new blogging site for&nbsp;Dell's engineers to interact with the public - announcing new designs, thoughts behind techniques, and more.&nbsp; It's heavy in video blogging and&nbsp;is staged to have&nbsp;great interaction with end-users</P>
<P><A href="http://one2one.dell.com/">one2one.dell.com</A></P>
<P>After reviewing the initial site after launch, something caught my eye.&nbsp; There was this video showing a silver and red box.&nbsp; At first I thought, "What is this thing?"&nbsp; Then I read the description of the blog post, talking about the XPS 700.&nbsp; Then I thought, "Wait, the XPS line has always been Dell's high-end workstations.&nbsp; But that doesn't look like a PC."</P>
<P><A href="http://one2one.dell.com/one2one/archive/2006/07/05/84.aspx" target=_blank><IMG src="/blog/archives/images/xps_700_dell_engineer.jpg" border=2></A></P>
<P><A href="http://one2one.dell.com/one2one/archive/2006/07/05/84.aspx">http://one2one.dell.com/one2one/archive/2006/07/05/84.aspx</A></P>
<P>Boy, have I been outta the loop.&nbsp; My head burried in the sand @ work, I need to get up-to-date on the&nbsp;latest tech.</P>
<P>Take a look at that thing!&nbsp; And check out the video there, and&nbsp;<A href="http://one2one.dell.com/one2one/archive/2006/07/05/79.aspx">another video&nbsp;in another post&nbsp;</A>at the site.&nbsp; Quad graphics cards anyone?</P>

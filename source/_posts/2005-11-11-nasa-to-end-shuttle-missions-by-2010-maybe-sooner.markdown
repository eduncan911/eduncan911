---
layout: post
title: "NASA to end Shuttle Missions by 2010 (maybe sooner)"
date: 2005-11-11 15:22:00 -0500
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Personal"]
alias: ["/blog/nasa-to-end-shuttle-missions-by-2010-maybe-sooner.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<p><img border="0" height="341" src="/blog/archives/images/international-space-station_high.jpg" width="500" /></p><p>The end of an era seems to be upon us.&nbsp; The aging Shuttle Program is being phased out by 2010.&nbsp; But if they do not return to space by Spring of 2006, the program will be canned for good.&nbsp; :(</p><p><a href="http://www.npr.org/templates/story/story.php?storyId=5007749" target="_blank">NPR</a> reported that NASA will be releasing a study in a few weeks looking into using Private Industry to cargo supplies to the ISS (International Space Station).&nbsp; They interview an Internet-Boom-Millionaire that is about to launch a rocket next year, for just $50 million vs. NASA&#39;s $1 billion price tag, per launch.</p><p>I closely watched the <a href="http://www.space.com/missionlaunches/xprize_full_coverage.html">Race to Space </a>a few years ago, and liked the direction SpaceShipOne idea.&nbsp; A lot of people protested that their design wasn&#39;t correct as it wasn&#39;t completely into space.&nbsp; Well with a few halogen nozzles to control direction, as with the X2 that Chuck Yeager flew oh so long ago, you can control it.</p><p>Besides, the whole idea behind the Race to Space was to find a cheap and plentiful solution to make it to space.&nbsp; And this shows that the private industry is very creative.</p><p>I think it&#39;s time.&nbsp; A time of a new era.&nbsp; Yes there will be disasters, but I&#39;d like to see Mars when I am 75.&nbsp; Wouldn&#39;t you?&nbsp; :)&nbsp; Ok, the Moon perhaps in a fly-by is more realistic.</p>

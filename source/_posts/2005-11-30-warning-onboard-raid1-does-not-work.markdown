---
layout: post
title: "WARNING: OnBoard RAID1 Does not Work!"
date: 2005-11-30 20:31:00 -0500
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff"]
alias: ["/blog/warning-onboard-raid1-does-not-work.aspx","/archive/2005/11/30/warning-onboard-raid1-does-not-work.aspx"]
---
{% include imported_disclaimer.html %}

<p><img align="left" height="304" src="/blog/archives/images/fooled.gif" width="389" /></p><p>For those of you running motherboards with SATA or IDE OnBoard RAID Controllers, you are all being fooled in thinking your data is safe.</p>

<!-- more -->

<p>Yesterday my machine started to become un-responsive when I was reading/writing a word&nbsp;document and an excel file.&nbsp; It would freeze/hang for about 10s, and start working again.&nbsp; This is a typical sign that something is wrong.&nbsp; I rebooted and immediately Chkdsk wants to check my drive D.&nbsp; I allow it, and it throws all sorts of index errors and inserts.&nbsp; So much that it gets caught in an endless loop, never to exit.</p><p>I have an Asus P5AD2-E Premium mobo - the company&#39;s flagship product for&nbsp;a short&nbsp;time.&nbsp; It has onboard SATA and IDE RAID.&nbsp; Actually two different RAID controllers - Silicon Image (Hardware) and an Intel (Software) controller.&nbsp; Each has 4 SATA ports.&nbsp; I did not want to use the Software raid, so I elected to setup my arrays on the Hardware version.</p><p>I have two WD 36 GB 10,000rpm SATA Rapters&nbsp;in an RAID0 array for my OS drive (C).<br />I <strong><em>had</em></strong> two Maxtor 200GB 7,200rpm SATA drives in an RAID1 array for my Data drive (D).</p><p>Trying my best to get Windows to boot, I finally get into the OS with no &quot;D&quot; drive and start checking the Raid&#39;s driver logs and event logs.&nbsp; Nothing.&nbsp; Nothing but a bunch of NTFS write errors from the previous boot.&nbsp; The raid drivers usually log to a log file in the C drive when there&#39;s an error - nothing is there.</p><p>I tried to connect one of the mirrored drives as a standard SATA drive on the Intel SATA ports (RAID is disabled on that part).&nbsp; I found the drive, everything seemed to be there.&nbsp; I connected the other mirrored drive.&nbsp; It saw it, and everything seemed to be ok.</p><p>I thought I was in the clear, until I started accessing my documents.&nbsp; Oh god!&nbsp; The majority of them are 0 bytes.&nbsp; In a panic, I went to check some of my oldest - most important files to me.&nbsp; GONE.&nbsp; They are not accessable.&nbsp; I shut hte system down, connected the other mirror&#39;d drive up in place of that HDD, booted and SAME EFFECT!&nbsp; The majority of my files are 0 bytes, corrupted, or invalid formats.</p><p>And just for completion sake.&nbsp; Yes, I tried the drives in another machine.&nbsp; Yes, I used Scan disk to search for errors/fix them (no problems found).&nbsp; Yes I used checkdsk /f /v&nbsp;on both disks and it did not find any errors.&nbsp; The drives seem to work just fine.&nbsp; And yes, the EXACT files are messed up on BOTH HDDs.</p><p>&nbsp;</p><p>Asus&#39; OnBoard RAID destroyed my files.&nbsp; From all angles and checks, the drives seems to be working just fine.&nbsp; I knew I should not have trusted a cheap OnBoard RAID solution.&nbsp; I wish I still had my Promise SATA controller.</p><p>&nbsp;</p>

---
layout: post
title: "The MiXX Collection for Community Server"
date: 2006-12-16 21:30:00 -0500
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Computer Programming", "Community Server", "PostIcon", "The MiXX Collection"]
alias: ["/blog/The-MiXX-Collection.aspx", "/blog/the-mixx-collection.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>Today I package up my MiXX tools into a collection of addons for Community Server.&nbsp; Right now, it only includes the PostIcon addon.&nbsp; At the end of this weekend, it should include a new MiXXVideoModule that is a major overhaul of the existing CS VideoModule.</P>
<P>(install only)<BR><A href="/files/folders/2611/download.aspx">/files/folders/2611/download.aspx</A><A href="/Downloads/MiXXCollection-CS30-v1.0.zip"></A></P>
<P>(source code-must compile)<BR><A href="/files/folders/5894/download.aspx">/files/folders/5894/download.aspx</A></P>
<P>This is the first of a number of major updates, finally releasing enhancements to Community Server I have sought after for many years.&nbsp;&nbsp;You can use the <A class="" href="/archive/tags/The+MiXX+Collection/default.aspx" mce_href="/archive/tags/The+MiXX+Collection/default.aspx"><IMG style="WIDTH: 18px; HEIGHT: 18px" height=18 src="/utility/images/feedicon.png" width=18 border=0 mce_src="/utility/images/feedicon.png">&nbsp;RSS Tag link&nbsp;to subscribe&nbsp;to the tag <STRONG><EM>The MiXX Collection</EM></STRONG></A> for the latest news of this tool.</P>
<P mce_keep="true"><img alt='The MiXX Collection for Community Server' src='http://images.google.com/images?q=tbn:2rHZ-0QvnbzZ2M:http://espn-att.starwave.com/media/pg3/2004/0325/photo/george_carlin275.jpg'/></P>

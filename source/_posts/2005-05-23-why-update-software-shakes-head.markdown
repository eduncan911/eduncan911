---
layout: post
title: "Why update software?  *shakes head*"
date: 2005-05-23 14:24:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff"]
alias: ["/blog/why-update-software-shakes-head.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>Why?&nbsp; Why update Adobe Reader when they release&nbsp;a new version?&nbsp; Oh, there's a new version of WinAMP.&nbsp; Hey look, my Quicktime is out of date.</P>
<P>Don't update.&nbsp; Almost all these products, when updated, become horrendously large&nbsp;filesizes and product size.&nbsp; Not to mention how they all are trying to sneak in some type of Toolbar or other applications for "Review".&nbsp; The most annoying always is the RealOne player.&nbsp; </P>
<P>Not to mention how these applications always think they are the best for playing ALL of your media, movie files, MP3s, etc so they&nbsp;always assume we want Real Player to open my mp3 files and play them.&nbsp; NO!!!</P>
<P>Not to mention all of that time you fought to remove them from the StartUp procedure (running in the tooltray, in the background for NO reason) is gone because when update them, they feel the need to load themselves in the background for "quicker launching".&nbsp; I'm sorry, I hardly every watch MOV files online.&nbsp; So why would I want QuickTime running in my tooltray, along with 15 other players and applications?</P>
<P>Now I'm talking updating software like media players, readers, Flash/multimedia, etc.&nbsp; Those little applications we need to browse the web daily and listen to audio files and such.&nbsp; Obviously mainstream applications need updates to fix bugs and large new features.</P>
<P>I have yet to see a reason for me to upgrade to the latest Adobe.&nbsp; I'm running Adobe Reader 5.0.&nbsp; Small, lightweight, loads fairly quickly.&nbsp; I really preferred version 4.0 of the reader as it was fast.&nbsp; But I have Adobe's PDF creator software, which is version 5.0.&nbsp; So I'm on 5.0 now.</P>
<P>I'm running WinAMP 2.61.&nbsp; No, that's not a misprint.&nbsp; Yes, I've been running the same verion since 2000.&nbsp; Why?&nbsp; Why not?&nbsp; All shoutcast streams work just fine, as well as MP3 playing.&nbsp; I don't want WinAMP to take over as my Media Player of choice, so there's no reason to upgrade.&nbsp; Heck, even plug-ins for WinAMP 5.0 work just fine in 2.61!&nbsp; Also, 2.61 doesn't force "reporting" or behind-the-scenes of contacting WinAMP to report back on statistics.&nbsp; It's extremely lightweight, and very non-intrusive.</P>
<P>AOL Instant Messenger.&nbsp; I'm not sure what the version number is, but I've been using the same messenger since 2000!&nbsp; Why?&nbsp; It's fast, lightweight, and <STRONG>doesn't have those annoying talking ads</STRONG>!&nbsp; Actually I no longer use AIM's client (nor ICQ or Yahoo) as running all of them was too bulky.&nbsp; I now use Trillian.&nbsp; And the only reason I keep updating Trillian is because one of the IM servers will change something, so I have to update to get a version that works.</P>
<P>Now RealOne needs to be slapped.&nbsp; I tried to run 8.0 of their player because they changed the name to RealOne.&nbsp; Then a lot of content because RealOne-only content so I had to update.&nbsp; Get this, Real has figured out a way to force users to upgrade.&nbsp; Their players expire!&nbsp; Even though I haven't seen one expire in the past 6 months (maybe the consumers got pissed off, like me).&nbsp; Perhaps that force upgrade was because it was the first RealOne release.</P>
<P><STRONG>Microsoft products</STRONG><BR>Ok, now these I do update as often as I can as they just get better and betters.&nbsp; Like Office and WMP and MSN Money, MSN Messenger,&nbsp;etc.&nbsp; I don't like how bloated Windows Media Player 10 has gotten, but they always seem to be improving performance, along with adding features (unlike ANY of the others above mentioned).&nbsp; With a little tweaking of the settings, WMP10 can load right up to display the library or Playing Now tab instead of the default Guide or What's Hot webpages (where it contacts MS and downloads content).&nbsp; Then it loads quickly.</P>
<P>I'd actually like to see Microsoft release a lightweight Media Player, like the old 6.4 version (that works with almost ALL content right now, btw!).&nbsp; Maybe call it Windows Media Player Light" that is strictly used for web browsing and streaming.&nbsp; Loading WMP10, the library of MP3 files I have, checking for updates, etc seems quite large for an application that I simply want to stream a Shoutcast stream with and run in the background while I work (and taking up almost 25MB of memory!).</P>
<P>WMP10 is still too bulky for me to load in the background while I work.&nbsp; So I still use WinAMP 2.61 in the background to stream my MP3 collection and stream the ShoutCast stations I've come to love.</P>
<P>&nbsp;</P>
<P>The moral of the story is&nbsp;if you see a popup that says, "Your X program is out of date" or "There is a newer version of X program", don't upgrade.&nbsp; Click the "Don't remind me" button and keep working.&nbsp; </P>

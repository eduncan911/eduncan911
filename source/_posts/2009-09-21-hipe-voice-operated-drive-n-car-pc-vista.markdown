---
layout: post
title: "HiPE’s voice-operated Drive-N car PC w/Vista"
date: 2009-09-21 06:55:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Automotive", "Geek Stuff", "Computer Programming", "Car Audio", "Car PC"]
alias: ["/blog/hipe-voice-operated-drive-n-car-pc-vista.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>This old <A href="http://www.engadget.com/2007/04/24/hipe-intros-voice-operated-drive-n-car-pcs-with-vista-mce/" target=_blank mce_href="http://www.engadget.com/2007/04/24/hipe-intros-voice-operated-drive-n-car-pcs-with-vista-mce/">Engadget post</A> reminds me of my Dell mini-pc I hacked up to fit in a block of foam and fans to run my XP on 12V with 80GB of MP3s and videos, with wireless (to sync from outside in the car).&nbsp; Too bad I never finished the project; but, I did sell it on eBay for a few hundred bucks.&nbsp; That guy wrote some flash interface for it.&nbsp; Nice.</P>
<P><A href="/blog/archives/images/HiPEsvoiceoperatedDriveNcarPCwVista_28F7/hipedriven.jpg" mce_href="/blog/archives/images/HiPEsvoiceoperatedDriveNcarPCwVista_28F7/hipedriven.jpg"><IMG style="BORDER-BOTTOM: 0px; BORDER-LEFT: 0px; DISPLAY: block; FLOAT: none; MARGIN-LEFT: auto; BORDER-TOP: 0px; MARGIN-RIGHT: auto; BORDER-RIGHT: 0px" title=hipe-drive-n border=0 alt=hipe-drive-n src="/blog/archives/images/HiPEsvoiceoperatedDriveNcarPCwVista_28F7/hipedriven_thumb.jpg" width=444 height=226 mce_src="/blog/archives/images/HiPEsvoiceoperatedDriveNcarPCwVista_28F7/hipedriven_thumb.jpg"></A> </P>
<BLOCKQUOTE>
<P><EM>“The units, ranging in price from $799 for a barebones Via C7 in an amp-style chassis up to $2199 for a tricked-out double-DIN touchscreen monster, all run Windows Vista MCE with the One Voice command system, and can be outfitted with GPS, WiFi, Bluetooth, XM, and even a rearview camera. The machines can also interface with your car's OBD II unit to let you monitor vehicle diagnostics, as well as clear out some of those annoying error messages you usually have to let the dealer handle.”</EM></P></BLOCKQUOTE>
<P>The ODB-II connection is nice.&nbsp; But still, $2k?</P>

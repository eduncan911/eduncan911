---
layout: post
title: "If you watch ads, you should get free stuff"
date: 2007-01-27 18:01:00 -0500
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Personal", "Funny", "Games"]
alias: ["/blog/if-you-watch-ads-you-should-get-free-stuff.aspx","/archive/2007/01/27/if-you-watch-ads-you-should-get-free-stuff.aspx"]
---
{% include imported_disclaimer.html %}

<P><IMG style="WIDTH: 300px; HEIGHT: 200px" align=left src="/blog/archives/images/storage/1000.4.3954.doom3-mcdonalds.jpg" width=300 height=200 mce_src="/p/storage/1000.4.3954.doom3-mcdonalds.jpg"></P>
<P>Warning, this is a rant.&nbsp; A rant against the gaming industry.&nbsp; I'm going to bash it hard.&nbsp; EA, turn away as you don't want to read this.</P>

<!-- more -->

<P>What's got me off my rocker tonight?&nbsp; Advertising within games.</P>
<P><A href="http://profile.myspace.com/index.cfm?fuseaction=user.viewprofile&amp;friendid=80352726" target=_blank mce_href="http://profile.myspace.com/index.cfm?fuseaction=user.viewprofile&amp;friendid=80352726">Morgan Webb</A> was just on <A href="http://www.g4tv.com/xplay/index.html" target=_blank mce_href="http://www.g4tv.com/xplay/index.html">G4tv's X-Play</A>&nbsp;bashing the gaming industry for pushing these Intel and Cingular ads on billboards, but the game makers still charge the full price for the game.&nbsp; She mentioned a few key points.</P>
<BLOCKQUOTE>
<P><EM>"Advertising within Halo on Xbox Live?&nbsp; It should be free to play then.&nbsp; Advertising for Cingular on a billboard as you crash your car into it at 120 mph on the bottle in a race car game?&nbsp; It should be $30, not $60 then to buy."</EM>&nbsp; </P></BLOCKQUOTE>
<P>So, she got me ramped up to night to flame the gaming industry.&nbsp; Morgan, I completely agree!</P>
<P><IMG style="WIDTH: 275px; HEIGHT: 200px" align=right src="/blog/archives/images/storage/1000.4.3957.splintercell-axe.jpg" width=275 height=200 mce_src="/p/storage/1000.4.3957.splintercell-axe.jpg">Me?&nbsp; Oh, I'm the last guy they wanted to piss off.&nbsp; I've paid, absolutely paid for every single game I have ever played.&nbsp; I was that kid who blew his $20 that his mother game him all in the arcade at the mall.&nbsp; I was that guy who stayed up all night playing the King Quests (which I beat&nbsp;2-5 btw, with no cheats).&nbsp; I was that kid who mowed so many lawns only to buy that game that everyone had.&nbsp; I'm that guy who plays games without the cheat codes as I want what I paid for.&nbsp; Currently in my office I have a number of boxes of the last few games I've played (counting 'em, 24).&nbsp; The latest?&nbsp; EA's Battlefield 2142.</P>
<P>EA Battlefield 2142 is EA's first attempt at online advertising using a company called IGA Worldwide, within the game.&nbsp; Ok, still haven't got the bigger picture yet?&nbsp; Say you are driving a car in the game.&nbsp; You come up onto a billboard on the side of the road.&nbsp; Battlefield 2142 is based into the future; the year 2142.&nbsp; But yet, you see an ad for Intel's Core 2 Duo plastered on the side.&nbsp; WTF.&nbsp; Or EA's current market advertising for the next game addon to be released, Battlefield 2143.</P>
<P><IMG style="WIDTH: 425px; HEIGHT: 270px" align=left src="/blog/archives/images/storage/1000.4.3955.counterstrike-subway.jpg" width=425 height=270 mce_src="/p/storage/1000.4.3955.counterstrike-subway.jpg">Then there was the scare that IGA was installing some type of spyware with Battlefield 2142.&nbsp; An EA rep recently informed the industry:</P>
<BLOCKQUOTE>
<P><EM>“The advertising program in</EM> Battlefield 2142 <EM>does not access any files which are not directly related to the game.&nbsp; It does not capture personal data such as cookies, account login detail, or surfing history.</EM>&nbsp;&nbsp; </P>
<P>BF 2142 <EM>delivers ads by region.&nbsp; The advertising system uses a player’s IP address to determine the region of the player, assisting to serve the appropriate ads by region and language.&nbsp;&nbsp; For instance, a player in Paris might be presented with ads in French.&nbsp; The information collected will not be repurposed for other uses.</EM>&nbsp;&nbsp; </P>
<P>Battlefield 2142 <EM>also tracks 'impression data' related to in-game advertisements: location of a billboard in the game, brand advertised, duration of advertisement impression, etc. This information is used to help advertisers qualify the reach of a given advertisement.”</EM></P></BLOCKQUOTE>
<P>Bandwidth.&nbsp; Most online gamers these days are on broadband.&nbsp; We all strive for the absolute lowest ping to the server.&nbsp; Well, these ads eat up a small chunk of internet bandwidth from your computer.&nbsp; While the bandwidth usually isn't the issue, it's the ping response time that is hindered.&nbsp;</P>
<P><IMG style="WIDTH: 320px; HEIGHT: 141px" align=right src="/blog/archives/images/storage/1000.4.3958.splintercell-sprite.jpg" width=320 height=141 mce_src="/p/storage/1000.4.3958.splintercell-sprite.jpg">The point Morgan and I are trying to make is if you are advertising in the game - GIVE US A FREAKIN BREAK ON THE PRICES!&nbsp; Don't charge full price for the game.&nbsp; Don't charge us for Xbox Live.&nbsp;</P>
<P>There's a flip side to this story though.&nbsp; Game developers are overworked and mostly underpaid for their skill level.&nbsp; Looking at Wikipedia's definition of <A href="http://en.wikipedia.org/wiki/In-game_advertising" target=_blank mce_href="http://en.wikipedia.org/wiki/In-game_advertising">in-game advertising</A>, it looks to be quite profitable ($56 mil last year and an estimated $1.8 billion by 2010).&nbsp; One might think that this is a great way to drive more <SPAN style="FONT-FAMILY: Arial; FONT-SIZE: 10pt; mso-fareast-font-family: 'Times New Roman'; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA">revenue</SPAN> to up the salaries of EA's developers.&nbsp; Yeah right.&nbsp; If that ever happens, you game developers let me know when your salaries double (aka come back into normal pay scales that match the rest of the industry).</P>
<P><IMG style="WIDTH: 200px; HEIGHT: 200px" align=left src="/blog/archives/images/storage/1000.4.3956.gta-fanta.jpg" width=200 height=200 mce_src="/p/storage/1000.4.3956.gta-fanta.jpg"></P>
<P mce_keep="true">&nbsp;</P>
<P>Oh and Morgan,&nbsp;I&nbsp;<IMG style="BORDER-BOTTOM: 0px; BORDER-LEFT: 0px; MARGIN: 0px; WIDTH: 19px; HEIGHT: 19px; BORDER-TOP: 0px; BORDER-RIGHT: 0px" src="/emoticons/emotion-25.gif" width=19 height=19 mce_src="/emoticons/emotion-25.gif"> u.&nbsp; ;)&nbsp; Hollar!</P>
<P mce_keep="true">&nbsp;</P>
<P>Edit: This post was originally written on December 14th, 2006.&nbsp; </P>
<P mce_keep="true"><img alt='If you watch ads, you should get free stuff' src='/blog/archives/images/storage/1000.4.3957.splintercell-axe.jpg'/></P>

---
published: true
layout: post
title: "Only 2 days left to crash the FCC"
description: "Open Comments on Net Neutrality ends in 2 days."
image: http://eduncan911.com/blog/images/fcc-net-neutrality-open-comments.jpg
video: https://www.youtube.com/watch?v=fpbOEoRrHyU
date: 2014-07-13 10:41:03 -0400
comments: true
categories: [technology, fcc]
tags: [fcc, government, "The Daily Show"]
---

[John Oliver Helps Rally 45,000 Net Neutrality Comments To FCC](http://www.npr.org/blogs/alltechconsidered/2014/06/03/318458496/john-oliver-helps-rally-45-000-net-neutrality-comments-to-fcc)

{% youtube fpbOEoRrHyU %}

There's only two days left for comments.  At the time I left mine a month ago, there were 65,000 comments.  Now, there are 205,000.

[The url to file your comments: fcc.gov/comments](http://www.fcc.gov/comments)

The YouTube video hit on every point I've been saying in person when talking about this, and many more.

One of my biggest I tell people is how the former chairman of the FCC left (forced out?), and the new one put in place by Obama last year previously ran (as in chaired and commanded) the lobbying firm for Cable and Wireless (e.g. Comcast and TimeWarner) which was directly responsible for the last attempt to create the two-tier system that the FCC blocked.  And, that sued the government to force this actual change.  

Translation by the current FCC chairman: "We didn't win last time. Ok, let's sue the FCC/government to force a rule change. When we win the lawsuit, I'll step down as head of this lobbyist group and become head of the FCC [so I can force this rule through]."

How fracked up is that?

Also it sucks that NetFlix already has to pay Comcast to get service to its users.


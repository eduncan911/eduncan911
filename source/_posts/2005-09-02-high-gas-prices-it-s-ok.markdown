---
layout: post
title: "High Gas Prices - It's ok"
date: 2005-09-02 03:06:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Automotive"]
alias: ["/blog/high-gas-prices-it-s-ok.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>Well, not really.&nbsp; But I heard an interview with someone on NPR today that kind of made me accept the higher prices much easier.<?xml:namespace prefix = o ns = "urn:schemas-microsoft-com:office:office" /><o:p></o:p></P>
<P>(let's see if I can interrupt/remember this properly, it's late and I've been working all day)<BR>Gas prices are priced similar to Supply and Demand.&nbsp; Well, our supply is much lower now (and getting even lower).&nbsp; We still have a supply&nbsp;but&nbsp;the supply <EM>is</EM> ~20% less (or was that 40%?).&nbsp; If our supply is less,&nbsp;our demand MUST be less (lowered).&nbsp; How do you&nbsp;lower the demand?&nbsp; Raise the prices&nbsp;to a point&nbsp;so only necessary people/trips are using gasoline.&nbsp; If there's not enough supply, raise it even higher.&nbsp; <o:p></o:p></P>
<P>This deters people from driving just 2 miles to their local grocery store (they can walk).&nbsp; This deters the teenagers driving on the "strip" Friday and Saturday nights.&nbsp; This deters just about any unnecessary driving all together.<o:p></o:p></P>
<P>I hate it as much as you guys, but&nbsp;they are&nbsp;right.&nbsp; We've got such a high demand for fuel that it's starting to kick us in the ass.<o:p></o:p></P>
<P>It's time to invest into alternative fuels.&nbsp; I've often thought about converting my <?xml:namespace prefix = st1 ns = "urn:schemas-microsoft-com:office:smarttags" /><st1:City w:st="on"><st1:place w:st="on">Lincoln</st1:place></st1:City> to Liquid&nbsp;Natural Gas (run on both gas or LNG).&nbsp; I think it's time to really research into this.<o:p></o:p></P>
<P>The Oil Men have enough for them and the next four generations&nbsp;to retire on (asses).&nbsp; Time to kick them to the curb.</P>
<P class=MsoNormal style="MARGIN: 0in 0in 0pt"><o:p>&nbsp;</o:p></P>

---
layout: post
title: "South Park wins the Peabody Award"
date: 2006-10-22 06:26:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Funny", "Southpark"]
alias: ["/blog/south-park-wins-the-peabody-award.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P><IMG style="WIDTH: 119px; HEIGHT: 170px" height=170 src="http://ccinsider.comedycentral.com/photos/uncategorized/71128149.jpg" width=119 align=left mce_src="http://ccinsider.comedycentral.com/photos/uncategorized/71128149.jpg">Congrats to the creators of South Park for the aware earlier this year (yeah I just found out).&nbsp; John Stewart here was the person who presented it to the creators, Trey Park and Matt Stone.&nbsp;&nbsp; If this doesn't open people's eyes to the humor they try to portray in today's media events, then I suppose it never will.</P>
<P>South Park was praised as a show that "pushes all the buttons, turns up the heat and shatters every taboo,"<A href="http://abcnews.go.com/Entertainment/wireStory?id=1809777" target=_blank mce_href="http://abcnews.go.com/Entertainment/wireStory?id=1809777"> Peabody Awards Director Horace Newcomb</A> said. "Through that process of offending it reminds us of the need for being tolerant." </P>
<P>Constantly while talking to several people, one out of every two seem to despise South Park for one thing or another.&nbsp; People still don't get&nbsp;the underlying humor.&nbsp; You can read in an earlier post about <A title="Profit Mohamed removed from South Park " href="/archive/2006/04/14/Death-to-Free-Speech_3A00_-Profit-Mohamed-removed-from-South-Park.aspx" mce_href="/archive/2006/04/14/Death-to-Free-Speech_3A00_-Profit-Mohamed-removed-from-South-Park.aspx">my stance on South Park</A> of where I go into more detail.</P>
<P>Until they start doing "You'd might be a redneck if..."-type jokes (as one Jeff Foxworthy mentioned has made me a redneck, lol), and one must hit home with me, I welcome their humor.&nbsp; And even if they do hit home, oh well.&nbsp; :)</P>
<P><img alt='South Park wins the Peabody Award' src='http://images.southparkstudios.com/img/content/news/2901a.jpg'/></P>

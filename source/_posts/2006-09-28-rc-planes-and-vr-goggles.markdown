---
layout: post
title: "RC Planes and VR Goggles"
date: 2006-09-28 03:09:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "RC Planes", "Hobbies"]
alias: ["/blog/RC-Planes-and-VR-Goggles.aspx", "/blog/rc-planes-and-vr-goggles.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<p><img alt='RC Planes and VR Goggles' src='http://images.google.com/images?q=tbn:ONrcWGg6g3wMxM:http://www.nasa.gov/images/content/58576main_ascan_training2.jpg'/></p><p>Back when I was into RC planes, I could never have imaged the combination of putting a camera into the cockpit and connecting a pair of VR goggles to create the ultimate viewpoint and flying control.</p><p><a href="http://video.google.fr/videoplay?docid=9091545735215129742&amp;hl=fr" target="_blank">On google videos is where I saw this</a>.</p><p>This is amazing.&nbsp; Yeah, call me a nerd for being into RC back in the day.&nbsp; But wow!&nbsp; Just look at the dives and fly-bys he does.&nbsp; Ok, I want one in a helo.</p>

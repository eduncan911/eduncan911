---
layout: post
title: "58 Facts You Probably Didn't Know About The Star Wars Movies"
description: "58 Facts You Probably Didn't Know About The Star Wars Movies"
image: http://s3-ec.buzzfed.com/static/2014-05/enhanced/webdr04/7/10/enhanced-5351-1399472292-3.jpg
video: https://www.youtube.com/watch?v=uJ3kV3Icm28
date: 2014-05-08 17:03:48 -0400
comments: true
categories: "lifestyle"
tags: "Star Wars"
---

[58 Facts You Probably Didn't Know About The Star Wars Movies](http://www.buzzfeed.com/awesomer/facts-you-probably-didnt-know-about-the-star-wars-movies)


{% blockquote %}

1. The actor who played Obi-Wan Kenobi, Alec Guiness, thought of the Star Wars films as “fairy-tale rubbish”.

2. Despite this, he negotiated a deal to earn 2% of the gross box office receipts for the movies he appeared in, earning him over $95 million.

3. Harrison Ford was paid $10,000 for his performance in Star Wars: Episode IV - A New Hope.

4. Peter Cushing, who played Grand Moff Tarkin, found his costume boots so uncomfortable that he wore slippers during many of his scenes, and insisted his feet just never be in the shots.

5. The sound of the TIE Fighter engines is actually the sound of an elephant call mixed with the sounds of a car driving on wet pavement.

6. Steven Spielberg made a bet with George Lucas for a percentage of the Star Wars films, which has earned him millions of dollars since.

7. While shooting the scene in the trash compactor, Mark Hamill held his breath for so long that he burst a blood vessel in the side of his face. They had to adjust framing while shooting the rest of the scene to avoid showing the blemish.

8. Many of the buildings constructed to be used in shots of Tatooine are still standing in Tunisia. In fact, some of them are still used by locals.

9. Denis Lawson, who played Wedge Antilles, is Ewan McGregor’s uncle.

10. Luke Skywalker was originally going to be named Luke Starkiller, and retained the name up until the film begin shooting. Luckily, the name was never mentioned, so it was changed to Skywalker with little effort.

11. The starship that became the Blockade Runner seen at the beginning of Star Wars: Episode IV - A New Hope was the original design for the Millennium Falcon.

12. The Jawa language is based on a sped-up version of the Zulu language.

13. The language Greedo speaks is a South American language called Quechua.

14. The bounty hunter Bossk’s clothing is a recycled spacesuit from Doctor Who.

15. Yoda’s species has never been named.

16. Mark Hamill was in a bad car accident before filming started on Star Wars: Episode V - The Empire Strikes Back, causing severe facial trauma. The scene in which Luke Skywalker is mauled by a Wampa was added to account for the scarring on his face.

17. Yoda was originally going to played by a monkey carrying a cane and wearing a mask.

18. During the evacuation of Cloud City, you can see an extra running with what appears to be an ice cream maker. The extra has since been given an elaborate backstory, and the supposed ice cream maker is meant to be a database of contacts within the Rebellion.

19. The word “ewok” is never said out loud in the Star Wars movies.

20. Luke’s lightsaber in Star Wars: Episode VI - Return of the Jedi was originally going to be blue to match the lightsaber he lost in the previous film, but George Lucas was worried that it would confuse audiences, and thought a green lightsaber would look better, so he made the change.

21. At one point, Star Wars: Episode VI - Return of the Jedi was going to be called “Revenge of the Jedi” and there were actually trailers and posters produced with the original title.

22. In fact, the producers of Star Trek II: The Wrath of Khan changed the name of their film from “Revenge of Khan” to avoid confusion between the two films.

23. The bounty hunter droid IG-88 was actually built from recycled film props. His head is the drink dispenser from the cantina scene in Star Wars: Episode IV - A New Hope.

24. Three of the aliens seen on Jabba’s barge in Star Wars: Episode VI - Return of the Jedi are named Klaatu, Barada, and Nikto. Their names are a referenced in Army Of Darkness as the words one must say to destroy the book of the dead. The names themselves are a reference to the words that must be spoken to shut down the robot in The Day the Earth Stood Still.

25. While filming Star Wars: Episode VI - Return of the Jedi, the codename for the project was Blue Harvest, which was supposed to be a horror film with the tagline “horror beyond imagination.”

26. The cast actually seriously considered making Blue Harvest when a series of sandstorms halted filming for several days.

27. Blue Harvest is a reference to the 1929 novel “Red Harvest,” which was the inspiration for the film Yojimbo, which itself was inspiration for the Star Wars films.

28. In one draft of Star Wars: Episode VI - Return of the Jedi, Obi Wan Kenobi and Yoda were going to leave the Force, and return to their physical bodies again to either assist Luke in his confrontation with Darth Vader and the Emperor, or to join him during the celebration on Endor.

29. Star Wars: Episode I - The Phantom Menace was labelled as “The Doll House” when it shipped to theaters.

30. No physical clone trooper outfits were actually produced for the films. Every clone trooper seen in the Star Wars films was created with CGI.

31. The communicator Qui-Gon Jinn uses is actually an altered Gillete Sensor Excel women’s razor.

32. Samuel L. Jackson claims that the words “bad motherfucker” were engraved on the lightsaber he used in the Star Wars films.

33. While filming lightsaber fight scenes, Ewan McGregor kept getting carried away and making the sounds of the weapon himself, which had to be removed in post-production.

34. Tupac Shakur auditioned for the role of Mace Windu.

35. An early draft of the Star Wars saga began with “This is the story of Mace Windu, a revered Jedi-bendu of Opuchi who was related to Usby C.J. Thape, a padawan learner of the famed Jedi.” It wasn’t until Star Wars: Episode I - The Phantom Menace that Mace Windu and Padawans first made an appearance.

36. The waterfalls cascading around the capital city of Naboo was actually salt.

37. Star Wars: Episode II - Attack of the Clones was labelled as “Cue Ball” when it shipped to theaters.

38. The cow-like creature seen grazing in the fields behind Anakin and Padmé in Star Wars: Episode II - Attack of the Clones can be seen again as an asteroid later in the film.

39. The members of NSYNC made a cameo in Star Wars: Episode II - Attack of the Clones to appease George Lucas’ daughters, but the scene was cut from the final version of the film.

40. Ahmed Best, the actor that plays Jar Jar Binks, makes an appearance out of costume in the background of one scene.

41. So does Anthony Daniels, who plays C-3PO.

42. George Lucas’ daughter Katie Lucas appears as a Twi’lek dancer in Star Wars: Episode II - Attack of the Clones.

43. Her sister, Amanda Lucas, appears as a background extra.

44. Their brother, Jett Lucas, appears as a young Padawan in the Jedi archives.

45. Star Wars: Episode III - Revenge of the Sith was labelled as “The Bridge” when it shipped to theaters.

46. While standing in on the Galactic Senate, Jar Jar Binks votes in favor of Order 66, leading to the destruction of the Jedi and the rise of the Galactic Empire. Even more reason to hate him.

47. The top-down shot of a severely burned Anakin Skywalker near the end of Star Wars: Episode III - Revenge of the Sith has the character framed within the symbol of the Galactic Empire.

48. The in-universe name for the genre of music heard during the cantina scene is “jizz.”

49. Anakin Skywalker/Darth Vader meets six of the nine diagnostic criteria for Borderline Personality Disorder, which is one more than is required to make the diagnosis.

50. Lucasfilm has someone on staff whose job is just to maintain Star Wars canon.

51. E.T.’s alien species are part of the Star Wars universe. A delegation of the aliens can be seen in the Galactic Senate.

52. In an early draft of the Star Wars story, R2-D2 speaks standard English, and is actually kind of a jerk.

53. George Lucas came up with the name R2-D2 while filming American Graffiti. A member of the sound crew asked him to retrieve reel #2 of the second dialogue track, which in the parlance would be, “Could you get R2-D2 for me?”

54. The phrase “I have a bad feeling about this” is said in every film.

55. There’s an island nation called Niue that accepts collectible Star Wars coins.

56. Every Star Wars film has been released the week after George Lucas’ birthday on May 14.

57. Anakin Skywalker/Darth Vader has been played by six different people: David Prowse, James Earl Jones, Bob Anderson, Sebastian Shaw, Jake Lloyd, and Hayden Christensen.

58. A disco version of the Star Wars theme became a No. 1 hit in 1977, and held the spot for two weeks.

{% youtube uJ3kV3Icm28 %}


{% endblockquote %}

And as a bonus, #32 ...

{% youtube ubyo5eZz_Jc %}

{% hattip Todd Major https://twitter.com/MeatAssassin %}
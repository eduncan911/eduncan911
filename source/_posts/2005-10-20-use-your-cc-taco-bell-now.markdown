---
layout: post
title: "Use your CC @ Taco Bell - now"
date: 2005-10-20 16:45:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: []
alias: ["/blog/use-your-cc-taco-bell-now.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>Finally, someone at TB got smart and realized all of the business they were loosing and ticked off customers they were having when they were charging you to use your debit card ($0.99 per transaction).&nbsp; I know I have walked outta TB several times cause I didn't have cash and was not going to pay this $1.00 fee.</P>
<P>Many other FF places accept CC/Debit, and do not charge.&nbsp; As a matter of fact, I can not think of another place that charges a fee!</P>
<P>Well, they are no longer charging.&nbsp; And they now accept CC as well as Debit.</P>
<P>So, gimmie my Grilled Stuffed Burrito!&nbsp; :)</P>
<P>&nbsp;</P>

---
layout: post
title: "Where did MP4 come from?"
date: 2006-07-07 19:18:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Computer Programming"]
alias: ["/blog/Where-did-MP4-come-from_3F00_.aspx", "/blog/where-did-mp4-come-from_3f00_.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2>Over the last few months, I've been seeing an increase of clients attempting to use MP4 files on their websites for videos.&nbsp; </FONT></SPAN><SPAN class=429430619-07072006><FONT face=Arial size=2>It's actually become an alarming amount, as the client may not be aware of the technical limitations in using this format.</FONT></SPAN></DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2></FONT></SPAN>&nbsp;</DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2>I wanted to announce to everyone how Microsoft and Telligent handles this format.</FONT></SPAN></DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2></FONT></SPAN>&nbsp;</DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2>First I know that MP4 is a good format is attempting to copyright your material.&nbsp; It's just that it hasn't caught on enough to be widely supported.</FONT></SPAN></DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2></FONT></SPAN>&nbsp;</DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2></FONT></SPAN>&nbsp;</DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial><STRONG>Microsoft and MP4</STRONG></FONT></SPAN></DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2></FONT></SPAN>&nbsp;</DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2>First, a snippet from Microsoft on how they will not support the MP4 format in Windows Media Player or alike.</FONT></SPAN></DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2></FONT></SPAN>&nbsp;</DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2>Source KB: <SPAN class=129081917-07072006><FONT face=Arial color=#0000ff size=2><A title=http://support.microsoft.com/default.aspx?scid=kb;en-us;Q316992 href="http://support.microsoft.com/default.aspx?scid=kb;en-us;Q316992">http://support.microsoft.com/default.aspx?scid=kb;en-us;Q316992</A></FONT></SPAN></FONT></SPAN></DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2><SPAN class=129081917-07072006></SPAN></FONT></SPAN>&nbsp;</DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2><SPAN class=129081917-07072006>============</SPAN></FONT></SPAN></DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2><SPAN class=129081917-07072006>
<H4 id=tocHeadRef><SPAN><A id=34></A></SPAN>MPEG-4 (.mp4)</H4>
<SCRIPT type=text/javascript>loadTOCNode(3, 'summary');</SCRIPT>
MPEG-4 is an International Standards Organization (ISO) specification that covers many aspects of multimedia presentation including compression, authoring and delivery. Although video compression and file container definition are two separate and independent entities of the MPEG-4 specification, many people incorrectly believe that the two are interchangeable. You can implement only portions of the MPEG-4 specification and remain compliant with the standard. <BR><BR>The MPEG-4 file format, as defined by the MPEG-4 specification, contains MPEG-4 encoded video and Advanced Audio Coding (AAC)-encoded audio content. It typically uses the .mp4 extension. Windows Media Player does not support the playback of the .mp4 file format. You can play back .mp4 media files in Windows Media Player when you install DirectShow-compatible MPEG-4 decoder packs. DirectShow-compatible MPEG-4 decoder packs include the Ligos LSX-MPEG Player and the EnvivioTV.<BR><BR>For more information about the Ligos LSX-MPEG Player, visit the following Ligos Web site: 
<DIV class=indent><A title=http://www.ligos.com/ href="http://www.ligos.com/" target=_blank>http://www.ligos.com</A><SPAN class=pLink> (http://www.ligos.com)</SPAN></DIV>For more information about EnvivioTV , visit the following Envivio Web site: 
<DIV class=indent><A title=http://www.envivio.com/products href="http://www.envivio.com/products" target=_blank>http://www.envivio.com/products/</A><SPAN class=pLink> (http://www.envivio.com/products)</SPAN></DIV>Microsoft has chosen to implement the video compression portion of the MPEG-4 standard. Microsoft has currently produced the following MPEG-4-based video codecs: 
<TABLE class="list ul">
<TBODY>
<TR>
<TD class=bullet>•</TD>
<TD class=text>Microsoft MPEG-4 v1</TD></TR>
<TR>
<TD class=bullet>•</TD>
<TD class=text>Microsoft MPEG-4 v2</TD></TR>
<TR>
<TD class=bullet>•</TD>
<TD class=text>Microsoft MPEG-4 v3</TD></TR>
<TR>
<TD class=bullet>•</TD>
<TD class=text>ISO MPEG-4 v1</TD></TR></TBODY></TABLE>MPEG-4 video content can be encoded and stored in an .asf file container by using Windows Media Tools and Windows Media Encoder. You can then play these files in Windows Media Player. For more information about Microsoft and MPEG-4 support, visit the following Microsoft Web site: 
<DIV class=indent><A title=http://www.microsoft.com/windows/windowsmedia/WM7/mpeg4.aspx href="http://www.microsoft.com/windows/windowsmedia/WM7/mpeg4.aspx">http://www.microsoft.com/windows/windowsmedia/WM7/mpeg4.aspx</A></DIV></SPAN></FONT></SPAN></DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2><SPAN class=129081917-07072006>============</SPAN></FONT></SPAN></DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2></FONT></SPAN>&nbsp;</DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2></FONT></SPAN>&nbsp;</DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial><STRONG>Telligent and MP4</STRONG></FONT></SPAN></DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2></FONT></SPAN>&nbsp;</DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2>And what does Telligent do for our clients?&nbsp; We'll easily support it, but it requires an server-side IIS change as well as their End-Users to know how to install the proper encoder.&nbsp; So if you are setting a client, and they will be using MP4, please ensure to setup the server to support raw MP4 file downloads.&nbsp; </FONT></SPAN></DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2></FONT></SPAN>&nbsp;</DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2>By default, MP4 files are not even recognized by Microsoft IIS.&nbsp; So&nbsp;you'll have to enable it.</FONT></SPAN></DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2></FONT></SPAN>&nbsp;</DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2>It is called a "<FONT face="Courier New">MIME Extension</FONT>" to add to the IIS website for the .MP4 extension.&nbsp; In addition, you'll have to know the ContentType to associate to it.</FONT></SPAN></DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2></FONT></SPAN>&nbsp;</DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2>Now the only ContentType I found most reliable with most 3rd party plug-in decoders out on the web is:</FONT></SPAN></DIV>
<BLOCKQUOTE dir=ltr style="MARGIN-RIGHT: 0px">
<DIV><SPAN class=429430619-07072006><FONT size=2><FONT face="Courier New">ContentType: video/<B>mp4</B></FONT></FONT></SPAN></DIV></BLOCKQUOTE>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2>But again, being how it isn't mixed support some clients may require specific overrides of this.</FONT></SPAN></DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2></FONT></SPAN>&nbsp;</DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2></FONT></SPAN>&nbsp;</DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2>I wanted to rehash on why this may not be the best idea for your clients.&nbsp; It's because the end user will require to install a 3rd party decoder to view it.&nbsp; Can you say DivX all over again?&nbsp; I still don't use DivX.</FONT></SPAN></DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2></FONT></SPAN>&nbsp;</DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2>Consider using a more widely support format, such as MPEG v2 or v3 or a compressable AVI.&nbsp; No, these are not the most efficient.&nbsp; But you will be able to view them on any platform with just about any type of player.</FONT></SPAN></DIV>
<DIV><SPAN class=429430619-07072006><FONT face=Arial size=2></FONT></SPAN>&nbsp;</DIV>

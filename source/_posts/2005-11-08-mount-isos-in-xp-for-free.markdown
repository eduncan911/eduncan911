---
layout: post
title: "Mount ISOs in XP - for free!"
date: 2005-11-08 01:40:00 -0500
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Computer Programming"]
alias: ["/blog/mount-isos-in-xp-for-free.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P>I just found the coolest little download from Microsoft.&nbsp;&nbsp;A free download to mount ISO files as drive letters (think MAC here).</P>
<P><A href="http://download.microsoft.com/download/7/b/6/7b6abd84-7841-4978-96f5-bd58df02efa2/winxpvirtualcdcontrolpanel_21.exe">Virtual CD-ROM Control Panel for Windows XP</A></P>
<P>Now THIS needs to be included in Vista, if it is not already.&nbsp; As the source of where I found it said, it's a little bit of a manual process and it's UnSupproted by MS (use at your own risk).&nbsp; It's got an ugly interface, but for Vista it should be a built-in right-click.</P>
<P>But we all know how slow VirtualPC is to install an OS from a CDROM.&nbsp; Using this tool I was able to load up W2K3 Server very quickly.&nbsp; </P>
<P>Tip: When running the virtual PC, make sure to go to CD -&gt; Use Physical Drive Z to enable the ISO to boot from.&nbsp; </P>
<P>Very very nice...</P>
<P>Thanks to <A href="http://weblogs.asp.net/pleloup/archive/2004/01/15/58918.aspx">pleloup</A>&nbsp;for finding this.&nbsp; </P>

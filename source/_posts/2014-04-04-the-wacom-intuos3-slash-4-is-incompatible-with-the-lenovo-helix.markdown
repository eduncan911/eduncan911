---
layout: post
title: "The Wacom Intuos3/4 is incompatible with the Lenovo Helix"
date: 2014-04-04 10:37:08 -0400
comments: true
categories: [hardware]
---

I recently abandoned my 14 year dedication to Dell laptops in favor of the
Lenovo Helix ultrabook/tablet.  In doing so, I needed a stylus for the active
digitizer as the one that came with the Helix just isn't going to work for
diagramming and sketching.

## Research: Styluses for the Lenovo Helix

I had a problem: I was obsessed for days reading every blog, review and comment
and watching every video I could find online to figure out what stylus would work
with the Wacom Active Digitizer that is built into the Helix.

Sadly, I couldn't find a straight answer.

## Wacom ZP501ESE Intuos3 Special Edition Grip Pen

This is the pen I decided on.  It arrived today and I immediately ripped it open
only to find out it didn't work.  And yes, I removed the OEM stylus cause there is
a switch that disables the digitizer when the pen is inserted to save battery.

## So which one?

Now that I am armed with new search queries *e.g. Lenovo Helix Intuos3 doesn't work*, 
I've found new pages with additional info that I wish I'd found in the first place.

Usually I am diligent enough to find these errors ahead of time.  But, I failed this
time around and I'm having fun writing on my new Octopress blog - so, why not whip
up a quick post to let others know.

I found [this thread](http://forums.lenovo.com/t5/X-Series-Tablet-ThinkPad-Laptops/Wacom-Intuos-4-pen-doesn-t-work-on-Helix/td-p/1153403/page/2) 
that was the most helpful in figuring out which stylus to get.  Specifically, this post:

{% blockquote Brett Gilbertson - Microsoft Tablet and Touch MVP http://forums.lenovo.com/t5/X-Series-Tablet-ThinkPad-Laptops/Wacom-Intuos-4-pen-doesn-t-work-on-Helix/td-p/1153403/page/2 Wacom Intuos 4 pen doesn't work on Helix %}
FYI, the Wacom Penabled Digitizer system is not compatible with Intuos, Cintiq, and Bamboo Tablet pens.
 
Any other Wacom Penabled Tablet PC Pen will work (as mentioned, Series 7 slate, Asus EP121, Lenovo ThinkPad Tablet 2, Lenovo X61 - X230T series, Fujitsu T900 series, Motion F5t, Wacom Bamboo Feel pens to name just a few).
 
Additionally the Wacom Pen Display Pens will work (eg DTU- series).
 
These pens come in different shapes, sizes and button configurations, but the pen must match up with the digitizer system that is built into the tablet and many people aren't aware that Wacom have different systems.
 
The Penabled system is not as advanced as the Intuos or Cintiq unfortunately (fewer pressure degrees, no tilt angles etc), but still very impressive and useful.
{% endblockquote %}

Well, there we have it.  NewEgg, a return is coming!

~E
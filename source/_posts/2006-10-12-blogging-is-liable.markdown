---
layout: post
title: "Blogging is Liable"
date: 2006-10-12 19:53:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Personal"]
alias: ["/blog/Blogging-is-Liable.aspx", "/blog/blogging-is-liable.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<p><img align="left" height="400" src="http://mattfitt.com/gallery2/d/21776-2/No_Free_Speech_1_web.jpg" style="padding-right: 8px; width: 267px; height: 400px" width="267" />It seems we are loosing our rights to free speech, more and more these days.&nbsp; Some may remember a previous post about <a href="/archive/2006/04/14/Death-to-Free-Speech_3A00_-Profit-Mohamed-removed-from-South-Park.aspx" target="_blank">SouthPark being sensored in showing the Profit Mohamed</a> last season.&nbsp; That was justified as it was a corporation that owned the cable channel, therefore sensoring anything they want.</p><p>Recently <a href="http://www.usatoday.com/tech/news/2006-10-02-bloggers-courts_x.htm" target="_blank">USAToday reported a story showing bloggers are liable</a> for what they say online.&nbsp; </p><p>Come on!&nbsp; When is this sh*t going to end?</p><p>I guess you need to make sure what you post is true, therefore never liable.&nbsp; But if the Government does not given all the facts, nor does anyone who tells a story of their one-sided issue they have with another party - are you never given all the facts.&nbsp; So speculation almost ensures being libel.&nbsp; This is just BS.</p>

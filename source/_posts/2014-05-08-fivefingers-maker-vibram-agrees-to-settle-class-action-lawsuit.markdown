---
layout: post
title: "FiveFingers maker Vibram Agrees to Settle Class Action Lawsuit"
description: 
image: http://eduncan911.com/blog/images/fivefingers_shoes.jpg
date: 2014-05-08 17:02:51 -0400
comments: true
categories: [lifestyle]
tags: [fivefingers, running]
---
[Vibram Agrees to Settle Class Action Lawsuit Claiming Company Deceived Consumers](http://www.runnersworld.com/general-interest/vibram-agrees-to-settle-class-action-lawsuit)

{% img /blog/images/fivefingers_shoes.jpg FiveFinger Vibram Shoes %}

{% blockquote Matt McCue http://www.runnersworld.com/general-interest/vibram-agrees-to-settle-class-action-lawsuit Vibram Agrees to Settle Class Action Lawsuit %}

Vibram USA, the company that makes FiveFingers running shoes, has agreed to settle a lawsuit that alleged the company made false and unsubstantiated claims about the health benefits of its glove-like footwear. According to the court filings, Vibram settled to put the matter to rest and avoid any additional legal expenses. “Vibram expressly denied and continues to deny any wrongdoing alleged in the Actions, and neither admits nor concedes any actual or potential fault, wrongdoing or liability,” read the court brief.

Valerie Bezdek brought the class action suit against Vibram in March 2012.  She filed her complaint in Massachusetts, the state where Vibram’s U.S. headquarters are located. Bezdek alleged that Vibram deceived consumers by advertising that the footwear could reduce foot injuries and strengthen foot muscles, without basing those assertions on any scientific merit. “The gist of her claim is that Vibram illegally obtained an economic windfall from her because it was only by making false health claims that Vibram induced consumers to buy FiveFingers shoes, and to pay more for them than they would have otherwise[.]”

{% endblockquote %}

I get the whole false claims thing that it could reduce foot injuries which wasn't based on any facts.  Just seems silly to see lawsuits like this after all this time based on mis-representation in advertising.  America, the land of the *litigated*.

A quick Google for [Valerie Bezdek](https://www.google.com/search?q=Valerie+Bezdek&num=50&es_sm=122&source=lnms&tbm=isch&sa=X&ei=hfhrU8uIJ6fesATAs4DgDQ&ved=0CAgQ_AUoAQ&biw=1211&bih=852) shows someone proud of her FiveFingers shoes.

Personally, I cannot live without my FiveFingers shoes.  I've had them for a little over 2 years now and keep using them (I got the extra durable ones).  Before getting them I was already [running barefooted on the balls of my feet](http://zenhabits.net/barefoot-running/), for proper technique.  I got these and they have allowed me to run in more places - again, on the balls of my feet, not my heel.  

I can easily see those that don't run on the balls of their feet getting injuries.

In the end, if you want to submit a claim the url will be:

http://www.fivefingerssettlement.com

In addition, it seems people who wear these shoes are thought of as snobs?  Give me a break.

{% hattip Todd Major https://twitter.com/MeatAssassin %}

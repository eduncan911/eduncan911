---
layout: post
title: "Make Love Not Warcraft"
date: 2006-10-05 02:52:00 -0400
comments: true
published: true
categories: ["blog", "archives"]
tags: ["Geek Stuff", "Funny", "Games", "Southpark"]
alias: ["/blog/Make-Love-Not-Warcraft.aspx", "/blog/make-love-not-warcraft.aspx"]
---
<!-- more -->
{% include imported_disclaimer.html %}
<P><IMG style="WIDTH: 200px; HEIGHT: 160px" align=left src="http://www.comedycentral.com/images/shows/southpark/videos/season_10/1008_warcraft_m4.jpg" width=200 height=160 mce_src="http://www.comedycentral.com/images/shows/southpark/videos/season_10/1008_warcraft_m4.jpg">7 weeks 5 days 13 hours 20 minutes - That's how long the boys from Southpark must play WoW, allowing 3 hours a night for sleep, in order to level up with their combined forces to have a 90% mortality rate against the ultimate super-player online.</P>
<P>I think what I just said about made some kind of sense to WoW players.&nbsp; </P>
<P>What had me rolling on the floor tonight, laughing my butt off?&nbsp; SouthPark.&nbsp; The 10th season premier was just on, and it had a huge spoof on&nbsp;the World of Warcraft online playing.&nbsp; </P>
<P>Basically, this one player has been online since WoW came out 18 months ago playing it almost 24/7.&nbsp; He's leveled up so high, that he's been killing players without having to accept his challenge - even killing admins off.</P>
<P>The show continues to spoof the "over the average" online player by showing the deterioration of health users go through if they do nothing but sit in front of the computer, 21 hours a day (3 hours to sleep) playing WoW.</P>
<P>Kind of reminds me about the <A href="http://www.youtube.com/watch?v=K8hfK3RQs2g" mce_href="http://www.youtube.com/watch?v=K8hfK3RQs2g">16 year old boy in&nbsp;Australia</A> that recently was on the news playing WoW, almost 18 hours a day.&nbsp; He's punched holes in the walls, thrown pc components around the room, and more.</P>
<P>Anyhoot, any decent WoW fan (or SouthPark fan) needs to check out this episode titled <EM>Make Love not Warcraft</EM>.</P>
<CENTER><EMBED height=400 type=application/x-shockwave-flash width=480 src=http://media.mtvnservices.com/mgid:cms:item:southparkstudios.com:104291 bgcolor="#000000" allownetworking="all" allowScriptAccess="always" allowFullScreen="true" flashVars="autoPlay=false&amp;dist=www.southparkstudios.com&amp;orig=" wmode="window"></EMBED> </CENTER>

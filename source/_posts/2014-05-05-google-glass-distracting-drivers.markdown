---
layout: post
title: "Google Glass Distracting Drivers"
image: http://eduncan911.com/blog/images/Google_Glass_Distracting_While_Driving.jpg
date: 2014-05-05 11:34:09 -0400
comments: true
categories: [technology]
tags: [Google, "Google Glass", Driving]
---
[Does Google Glass Distract Drivers? The Debate Is On](http://www.npr.org/blogs/alltechconsidered/2014/03/24/289802359/does-google-glass-distract-drivers-the-debate-is-on)

{% img /blog/images/Google_Glass_Distracting_While_Driving.jpg Google Glass Driving %}

I have several friends, CEOs, CTOs and even strangers tell me they can talk and drive, they can text and drive, they are good drivers, etc.

I've always rebuttaled that the human brain can only focus on a single context, at any given time.

{% audio http://pd.npr.org/anon.npr-mp3/npr/atc/2014/03/20140324_atc_does_google_glass_distract_drivers_the_debate_is_on.mp3 04:42 NPR: Does Google Glass Distract Drivers? %}

A quote in this story summed it up quite well:

{% blockquote Does Google Glass Distract Drivers? The Debate Is On http://www.npr.org/blogs/alltechconsidered/2014/03/24/289802359/does-google-glass-distract-drivers-the-debate-is-on %}

Earl Miller, a professor of neuroscience at MIT who specializes in multitasking, says this sounds like wishful thinking.

"You think you're monitoring the road at the same time, when actually what you're doing [is] you're relying on your brain's prediction that nothing was there before, half a second ago — that nothing is there now," he says. "But that's an illusion. It can often lead to disastrous results."

In other words, the brain fills in the gaps in what you see with memories of what you saw a half-second ago. Among scientists, that statement is not controversial. The politics of Google Glass — and where it's worn — clearly is.

{% endblockquote %}

Bingo.  And this includes "hands-free" conversations as well as GPS.

Then again, in 30 years we may evolve to actually have our brains multi-task.

{% hattip ForgetFoo http://forgetfoo.com %}
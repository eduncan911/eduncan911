---
layout: page
title: "Privacy Policy"
date: 2014-04-10 02:36
comments: false
sharing: false
footer: true
---
## What? Why Have A Privacy Policy?

Well, you wouldn't want eduncan911.com sharing all sorts of deeply personal information about you, would you? Well of course not. Besides, some affiliate programs require a privacy policy. So here you go, a privacy policy.

## Besides Your Rear, What Does The Policy Cover?

This privacy policy covers how eduncan911.com treats personal information it receives from visitors. It gives the average reader the assurance that his or her personal information is safe from Big Brother and other prying eyes.

## How Do You Guarantee Privacy?

As in life, there are no perfect guarantees. However, as eduncan911.com doesn't have registration, it really doesn't have much information to share in the first place.

## What Information Do You Collect?

This site doesn't itself collect data. It's a static site hosted on GitHub Pages.  [Here, go have a look at the complete source code.](https://github.com/eduncan911/eduncan911.github.io)  However, I do use Google Analytics so it's worth looking at Google Analytic's privacy policy. I also use Disqus for comments, so take a look at the Disqus's privacy policy.

Both the aforementioned companies and technologies are hosted outside of this website and are out of my control.

## What About Cookies?

See the previous answer about Disqus and Google Analytics.  

## So Who Do You Share This Information With?

Absolutely nobody, other than occasionally posting anonymized aggregate stats about this site.

## Even If I Offered You 10 Million Dollars?

Sure, if I had anything to sell.  Since this site doesn't collection any information, there is nothing to sell though.
---
layout: page
title: "buy me a beer"
date: 2014-04-06 23:15
comments: false
sharing: false
footer: true

---
Clicking the image below shoots you to PayPal for a small $5 USD donation.

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_s-xclick" />
<input type="hidden" name="hosted_button_id" value="493LX823DBR4N" />
<input type="hidden" name="item_name" value="eduncan911com" />
<input type="hidden" name="" value="" />
<input type="hidden" name="" value="" />
<input type="hidden" name="" value="" />
<input type="hidden" name="" value="" />
<input type="image" src="/images/upload/buy-me-a-beer.jpg" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" />
<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1" />
</form>

## Other ways to help out

### Ting $25 Credit

[{% img http://1.bp.blogspot.com/-Oti5LupeGCo/UR_VfZiN-uI/AAAAAAAAASU/1AYsQUt73EI/s1600/tingdashboard.jpg 'Ting me and get $25 credit!' 'Ting me and get $25 credit!' %}](https://zc7c9715lq2.ting.com/)

I switched my family from Sprint to Ting around May 2013 and have saved $980 so far
as of April 2014.  How are your cell phone bills?

<blockquote class="twitter-tweet" lang="en"><p>Awesome... Another <a href="https://twitter.com/tingFTW">@TingFTW</a> referral. That&#39;s now almost two months free cell service for my family! Thx whoever u are!</p>&mdash; Eric Duncan (@eduncan911) <a href="https://twitter.com/eduncan911/statuses/453898728621867011">April 9, 2014</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

You can get a $25 credit on any new device or towards your first bill
by using my affiliate code.  I will also get a $25 credit as well for referring
you!  It's a win-win.

Once you go Ting, you'll never go back.

To get $25 off, use my referral code by clicking [https://ting.com/r/zc7c9715lq2](https://ting.com/r/zc7c9715lq2).

### Amazon Affiliate

[{% img https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQhXcDwTzSdXQSaT8z-MeO-sPTh2ryqg2xQZWYgHc1_31h7n7VJ9w %}](http://www.amazon.com/?tag=eduncan911-20)

If you shop Amazon a lot, you can use my referral code on your next purchase.  
It's pretty simple:

1. Come back to this page when you are ready to make a purchase
2. Click the Amazon banner above.
3. Make your purchase.

You can verify what I am doing by looking at the url.  You should see
something like:

`www.amazon.com?tag=eduncan911-20`

### DNSimple for DNS

[{% img http://jasonseifer.com/assets/dnsimple.png 'DNSimple 2 months of free service' %}](https://dnsimple.com/r/101251903e88bc)

I have been purchasing domains since 1996 and using 3rd party DNS servers since 2002.
By far the best and most inexpensive I've used is DNSimple.  No more GoDaddy DNS 
outages, no more DNS delays - they really serve up to 1 minute TTL!

By using my referral, signing up and when you active your account you'll have 
two months of free service.

To use my referral and get two-months free, click [https://dnsimple.com/r/101251903e88bc](https://dnsimple.com/r/101251903e88bc).


